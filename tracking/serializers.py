from rest_framework.serializers import (IntegerField, Serializer, SerializerMethodField,
                                        DecimalField, DateTimeField, ModelSerializer,
                                        ReadOnlyField, FloatField)
from .models import Viaje, Recorrido, Tracking
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from geojson_serializer.serializers import geojson_serializer


class DataSet(object):
    """
    Objeto para serializar datos externos a los modelos
    """
    def __init__(self, **kwargs):
        for field in ('registrados', 'no_registrados', 'en_punta_de_lineas',
                        'adaptados_activos', 'adaptados_en_servicio'):
            setattr(self, field, kwargs.get(field, None))


class DatosInternosSerializer(Serializer):
    registrados = IntegerField(read_only=True)
    no_registrados = IntegerField(read_only=True)
    en_punta_de_lineas = IntegerField(read_only=True)
    adaptados_activos = IntegerField(read_only=True)
    adaptados_en_servicio = IntegerField(read_only=True)

    def create(self, validated_data):
        return DataSet(registrados=None,
                        no_registrados=None,
                        en_punta_de_lineas=None,
                        adaptados_activos=None,
                        adaptados_en_servicio=None
                        **validated_data)

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)
        return instance


class ViajeSerializer(ModelSerializer):
    nombre_interno = SerializerMethodField()
    nombre_linea = SerializerMethodField()
    score = DecimalField(required=False, max_digits=10, decimal_places=2)
    iniciado = DateTimeField(format="%H:%M:%S", required=False, read_only=True)
    terminado = DateTimeField(format="%H:%M:%S", required=False, read_only=True)
    # iniciado = DateTimeField(format="%d-%m-%YT%H:%M:%S", required=False, read_only=True)


    def get_nombre_linea(self, obj):
        if obj.recorrido is None or obj.recorrido.linea is None:
            return ''
        else:
            return obj.recorrido.linea.nombre_publico

    def get_nombre_interno(self, obj):
        if obj.interno is None:
            return ''
        else:
            return obj.interno.codigo_vehiculo


    class Meta:
        model = Viaje
        fields = ('id', 'nombre_interno', 'score', 'nombre_linea', 'iniciado', 'terminado', 'recorrido_id')


class TrackingSerializer(GeoFeatureModelSerializer):
    """Clase para serializar el track de un viaje"""
    # distancia = FloatField()

    class Meta:
        model = Tracking
        # fields = ('timestamp',)
        fields = ('timestamp',)
        geo_field = "location"


class RecorridoSerializer(GeoFeatureModelSerializer):
    """Clase para serializar las trayectorias (recorridos) """

    class Meta:
        model = Recorrido
        fields = ('descripcion_corta', 'descripcion_larga', 'descripcion_cuando_llega', 'recorrido_id')
        geo_field = "trazado_extendido"


@geojson_serializer('point')
class PuntoTrayectoriaSerializer(Serializer):
    point = ReadOnlyField()
    desvio = DecimalField(required=False, max_digits=10, decimal_places=3)


class DesviosSerializer(Serializer):
    recorrido = PuntoTrayectoriaSerializer(many=True)
    viaje = TrackingSerializer(many=True)
    media = FloatField(required=False)
    indice_validacion = FloatField(required=False)