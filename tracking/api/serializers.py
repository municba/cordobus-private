from rest_framework.serializers import (ModelSerializer, CharField,
                                        IntegerField, StringRelatedField,
                                        SerializerMethodField, Serializer,
                                        FloatField, ReadOnlyField, DecimalField,
                                        DateTimeField)
from rest_framework_gis.serializers import GeoFeatureModelSerializer, GeometryField
from tracking.models import *


class InternosActivosSerializer(GeoFeatureModelSerializer):
    nombre = CharField(source='name')
    linea = SerializerMethodField()
    recorrido_nombre = SerializerMethodField()
    color = CharField(source='empresa.color')

    def get_linea(self, obj):
        if obj is None:
            return None
        elif obj.recorrido_actual is None:
            return None
        elif obj.recorrido_actual.linea is None:
            return None
        return obj.recorrido_actual.linea.nombre_publico if obj.recorrido_actual.linea.nombre_publico != '' else 'Desconocido'

    def get_recorrido_nombre(self, obj):
        if obj is None:
            return None
        elif obj.recorrido_actual is None:
            return None
        return obj.recorrido_actual.descripcion_corta if obj.recorrido_actual.descripcion_corta != '' else 'Desconocido'

    class Meta:
        model = InternoGPS
        geo_field = 'ultima_posicion_conocida'
        fields = ('id','nombre', 'linea', 'ultima_posicion_time', 'recorrido_actual', 'recorrido_nombre',
                    'ultimas_velocidades_promedio', 'ultimas_variaciones_latitud',
                    'ultimas_variaciones_longitud', 'adaptado', 'piso_bajo',
                    'articulado', 'empresa', 'color')


class RegistroDeFrecuenciaSerializer(ModelSerializer):
    momento = SerializerMethodField()
    linea_nombre = SerializerMethodField()
    cantidad_unidades_circulando = IntegerField()
    unidades_esperadas = IntegerField()


    def get_momento(self, obj):
        return obj.momento.timestamp
    
    def get_linea_nombre(self, obj):
        return obj.linea.nombre_publico

    class Meta:
        model = RegistroDeFrecuencia
        fields = ('momento', 'linea_nombre', 'cantidad_unidades_circulando', 'unidades_esperadas')