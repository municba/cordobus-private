from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import DjangoModelPermissions, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ReadOnlyModelViewSet, ViewSet
from core.pagination import DefaultPagination
from .serializers import (DatosInternosSerializer, DataSet,
                            ViajeSerializer, RecorridoSerializer,
                            DesviosSerializer)
from .models import InternoGPS as Interno, Viaje, Recorrido, Tracking
from lineas.models import Recorrido
from rest_framework.response import Response
from django_filters import rest_framework as filters
import datetime
from rest_framework.decorators import action
from common.helpers.desvios import desvios_modelos, validar_viaje_modelo
from django.contrib.gis.geos import Point
import logging
logger = logging.getLogger(__name__)


class DatosInternosViewSet(ViewSet):
    """
    Datos extras de los internos
    """
    permission_classes = (IsAuthenticatedOrReadOnly, )
    serializer_class = DatosInternosSerializer
    pagination_class = DefaultPagination

    def list(self, request):
        #excluyo los no logueados
        activos_ahora = Interno.objects.activos_ahora()  # no incluye los en zona de punta de linea
        logueados = Interno.objects.registrados().count()
        no_logueados = Interno.objects.no_registrados().count()
        en_punta_de_linea = Interno.objects.en_zona_de_fuera_de_servicio().count()
        adaptados_activos = Interno.objects.adaptados_activos().count()
        adaptados_en_servicio = Interno.objects.adaptados_en_servicio().count()
        
        dataset = {1: DataSet(registrados=logueados,
                                no_registrados=no_logueados,
                                en_punta_de_lineas=en_punta_de_linea,
                                adaptados_activos=adaptados_activos,
                                adaptados_en_servicio=adaptados_en_servicio)}
        serializer = DatosInternosSerializer(instance=dataset.values(), many=True)
        return Response(serializer.data)


class ViajeFilter(filters.FilterSet):
    """Filtros que se pueden aplicar para poder listar viajes
    Ejemplo: https://cordobus.apps.cordoba.gob.ar/data/viajes/?iniciado_date=2019-04-09&linea=28
    """

    linea = filters.NumberFilter(field_name="recorrido__linea__id")
    iniciado_date = filters.DateTimeFilter(method='all_day_filter')
    terminado_date = filters.DateTimeFilter(method='all_day_filter')

    def all_day_filter(self, queryset, name, value):
        filter_atribute = name.split("_")[0] + '__range'
        return queryset.filter(**{
            filter_atribute: (datetime.datetime.combine(value, datetime.time.min),
                            datetime.datetime.combine(value, datetime.time.max)),
        })

    class Meta:
        model = Viaje
        fields = {
            'interno':['exact'],
            'recorrido':['exact'],
            'linea': ['exact'],
            'iniciado':['exact', 'lt', 'gt', 'lte', 'gte'],
            'iniciado_date':['exact'],
            'terminado': ['exact', 'lt', 'gt', 'lte', 'gte'],
            'terminado_date': ['exact'],
        }


class ViajeViewSet(ReadOnlyModelViewSet):
    """Api para listar los viajes registrdos en el modelo InternoEnRecorrido
    
    retrieve:
        Retorna los datos de un viaje, sin sus puntos del recorrido

    list:
        Retorna una lista de viajes filtrados por los parametros pasado

    filters:
            interno (int): Id de un interno.
            recorrido (int): Id de un recorrido.
            linea (int): Id de una linea
            ultimo_vez (datetime): fecha y hora de ultima vez
    """
    serializer_class = ViajeSerializer
    authentication_classes = (SessionAuthentication, )
    permission_classes = (IsAuthenticated, )
    # TODO: esto se podria agregar por defecto al manager si es que realmente no tiene sentido que
    # falten estos atributos
    queryset = Viaje.objects.filter(interno__isnull=False, recorrido__isnull=False )
    pagination_class = None
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ViajeFilter

    def list(self, request):
        # aplicar cuentas sobre datos en el queryset filtrado
        filtro = ViajeFilter(request.GET, queryset=self.queryset)
        viajes_qs = filtro.qs

        # TODO: si se recibe un parametro que indica que hay que recalcular, habria que aplicar para los viajes con resultados
        # se recorren los que no tengan resultados y se calculan persistiendolos
        viajes_sin_resultados = viajes_qs.filter(fecha_calculo__isnull=True)
        viajes_sin_resultados_calculados = viajes_sin_resultados.count()
        max_viajes_sin_resultados_calculados = 250
        if viajes_sin_resultados_calculados < max_viajes_sin_resultados_calculados:
            
            """TODO: hay que poner un limite para realizar el calculo siguiente, 
            momentaneamente se pone 250. Igualmente la respuesta sigue existiendo
            pero sin el cálculo
            """
            for viaje in viajes_sin_resultados:
                viaje_filtrado = Tracking.objects.viaje_filtrado(viaje)

                try:
                    desvios_sobre_recorrido = desvios_modelos(viaje_filtrado, viaje.recorrido)
                except Exception as e:
                    logger.error('Error al calcular desvíos: {}'.format(e, viajes_sin_resultados_calculados))
                    continue

                viaje.score = desvios_sobre_recorrido.mean()
                viaje.fecha_calculo = datetime.datetime.now()
                viaje.save()

        else:
            logger.error('HAY MAS DE {} viajes sin score calculado: {}'.format(max_viajes_sin_resultados_calculados, viajes_sin_resultados_calculados))

        return super(ViajeViewSet, self).list(request)

    @action(methods=['get'], detail=True)
    def tracking(self, request, pk=None):
        """
        Obtiene el tracking de un viaje
        :param request: peticion
        :param pk: identificador del viaje
        :return: TrackingSerializer
        """
        viaje = self.get_object()

        viaje_qs = Tracking.objects.filter(viaje=viaje)
        serializer = TrackingSerializer(viaje_qs, many=True)

        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def desvios(self, request, pk=None):
        viaje = self.get_object()

        viaje_qs = Tracking.objects.viaje_filtrado(viaje)
        
        desvios_sobre_recorrido = desvios_modelos(viaje_qs, viaje.recorrido)
        indice_validacion = validar_viaje_modelo(viaje_qs, viaje.recorrido)

        ptos_recorrido = [{'point': Point(pto[0], pto[1]), 'desvio': desvios_sobre_recorrido[index]}
                            for index, pto in enumerate(reversed(viaje.recorrido.trazado_extendido.coords))]

        ret = {
            'recorrido': ptos_recorrido,
            'viaje': viaje_qs,
            'media': desvios_sobre_recorrido.mean(),
            'indice_validacion': indice_validacion
        }
        serializer = DesviosSerializer(ret)
        return Response(serializer.data)
    

class RecorridoViewSet(ReadOnlyModelViewSet):

    serializer_class = RecorridoSerializer
    queryset = Recorrido.objects.all()
    pagination_class = DefaultPagination

    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('recorrido_id',)
