from django.contrib.auth.decorators import permission_required, login_required
from django.views.generic import TemplateView
from lineas.models import Recorrido


class EditorView(TemplateView):
    template_name = "editor_paradas.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(EditorView, self).get_context_data(**kwargs)
        context['recorridos'] = Recorrido.objects.filter(publicado=True).order_by('linea__nombre_publico', 'descripcion_corta')
        return context


class MapParadasView(TemplateView):
    template_name = 'paradas/editar-bondis-en-mapa.html'

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['recorridos'] = Recorrido.objects.all().order_by('linea__nombre_publico', 'descripcion_corta')
        return context
