from django.urls import path
from django.conf.urls import url
from .views import MapParadasView, EditorView


urlpatterns = [
    url(r'^mapa-editor/$', MapParadasView.as_view(), name='mapa-editor'),
    path('editor-paradas', EditorView.as_view(), name='editor-paradas'),
]
