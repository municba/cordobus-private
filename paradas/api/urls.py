from django.conf.urls import url, include
from rest_framework import routers
from .views import *


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'paradas-colectivos', ParadaViewSet, base_name='paradas.api.lista')
router.register(r'postes-paradas-colectivos', PosteParadaGeoViewSet, base_name='poste-paradas-colectivos.api.lista')

# router.register(r'postes-con-paradas', PosteSimpleParadaTransportePublicoViewSet, base_name='postes-con-paradas.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
