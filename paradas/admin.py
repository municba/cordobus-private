from django.contrib import admin
from .models import PosteParada, Parada
from core.admin import QhapaxOSMGeoAdmin


@admin.register(PosteParada)
class PosteParadaTransportePublicoAdmin(QhapaxOSMGeoAdmin):
    search_fields = ['id', 'id_externo', 'descripcion', 'identificador']
    list_display = ('id', 'id_externo', 'identificador', 'descripcion')


@admin.register(Parada)
class ParadaAdmin(QhapaxOSMGeoAdmin):

    def linea(self, obj):
        linea_str = obj.recorrido.linea.nombre_publico if obj.recorrido is not None else 'Sin línea asociada'
        return  linea_str

    search_fields = ['poste__descripcion', 'codigo',
                        'recorrido__descripcion_corta',
                        'recorrido__descripcion_larga',
                        'recorrido__descripcion_interna']
    list_display = ('codigo', 'poste', 'linea', 'recorrido', 'distancia_a_recorrido', 'verificado')
    list_filter = ['recorrido', 'verificado']