from django.contrib.gis.db import models


class PosteParada(models.Model):
    """ cada una de las garitas o postes donde puede haber una o más paradas """
    id_externo = models.CharField(max_length=90, null=True, blank=True, help_text='ID en el webservice de gobierno abierto córdoba')
    descripcion = models.CharField(max_length=90, null=True, blank=True)
    ubicacion = models.PointField(null=True, blank=True, help_text='Punto exacto de ubicación del poste')
    identificador = models.CharField(max_length=90, null=True, blank=True, help_text='ID en el webservice de EFISAT')

    def __str__(self):
        return '{} ({})'.format(self.id_externo, self.descripcion)

    class Meta:
        verbose_name = 'Poste'
        verbose_name_plural = 'Postes'


class Parada(models.Model):
    """ cada una de las lineas o recorridos de transporte publico masivo """
    codigo = models.CharField(max_length=50, help_text='Codigo que viene desde el proveedor', null=True, blank=True)
    poste = models.ForeignKey(PosteParada, related_name='paradas', on_delete=models.CASCADE)
    recorrido = models.ForeignKey('lineas.Recorrido', related_name='paradas', on_delete=models.CASCADE, null=True, blank=True)

    distancia_a_recorrido = models.DecimalField(
        max_digits=10, decimal_places=4, null=True, blank=True,
        help_text=(
            "Distancia en metros que calcula el sistema entre el poste y el punto "
            "más cercano del recorrido al que pertenece. Si la distancia es muy grande, "
            "controlar que esté en el recorrido correcto y marcar como verificado."
        )
    )
    verificado = models.BooleanField(
        default=True,
        help_text='Marcar como falso si la parada dejó de existir.'
    )

    def __str__(self):
        return '{} ({})'.format(self.poste.id_externo, self.recorrido.linea.id_externo)

    def save(self, *args, **kwargs):
        #FIXME actualizar la "distancia_a_recorrido" de esta parada
        super().save(*args, **kwargs)

    class Meta:
        unique_together = (("poste", "recorrido"),)
        permissions = (
            ('map_editor', 'Puede editar paradas en mapa'),  #FIXME agregar el editor de paradas a este proyecto
        )
