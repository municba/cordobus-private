from core.pagination import DefaultPagination
from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import ContratoUsoBoletoSerializer
from boletos.models import ContratoUsoBoleto



class ContratoUsoBoletoViewSet(viewsets.ModelViewSet):
    """
    Contratos de los boletos
    """

    serializer_class = ContratoUsoBoletoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = ContratoUsoBoleto.objects.all()

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
