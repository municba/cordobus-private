from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ViewSet
from api.pagination import DefaultPagination
from api.serializers import DatosInternosSerializer
from tracking.models import InternoGPS
from rest_framework.response import Response


class DatosInternosViewSet(ViewSet):
    """
    Datos extras de los internos
    """
    permission_classes = (IsAuthenticatedOrReadOnly, )
    serializer_class = DatosInternosSerializer
    pagination_class = DefaultPagination

    def list(self, request):
        #excluyo los no logueados
        activos_ahora = InternoGPS.objects.activos_ahora()  # no incluye los en zona de punta de linea
        logueados = InternoGPS.objects.registrados().count()
        no_logueados = InternoGPS.objects.no_registrados().count()
        en_punta_de_linea = InternoGPS.objects.en_zona_de_fuera_de_servicio().count()
        adaptados_activos = InternoGPS.objects.adaptados_activos().count()
        adaptados_en_servicio = InternoGPS.objects.adaptados_en_servicio().count()
        
        dataset = {1: DataSet(registrados=logueados,
                                no_registrados=no_logueados,
                                en_punta_de_lineas=en_punta_de_linea,
                                adaptados_activos=adaptados_activos,
                                adaptados_en_servicio=adaptados_en_servicio)}
        serializer = DatosInternosSerializer(instance=dataset.values(), many=True)
        return Response(serializer.data)

