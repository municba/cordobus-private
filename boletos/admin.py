from django.contrib import admin
from .models import Chofer, TarjetaRedBus, ContratoUsoBoleto, ZonaBoleto



@admin.register(Chofer)
class ChoferAdmin(admin.ModelAdmin):
    search_fields = ['id_chofer_worldline']
    list_filter = ['empresa']
    list_display = ('id', 'id_chofer_worldline', 'empresa')


@admin.register(TarjetaRedBus)
class TarjetaRedBusAdmin(admin.ModelAdmin):
    search_fields = ['numero_tarjeta']
    list_filter = ['ultimo_contrato']
    list_display = ('numero_tarjeta', 'ultimo_saldo', 'usos_remanentes', 'ultimo_contrato')


@admin.register(ContratoUsoBoleto)
class ContratoUsoBoletoAdmin(admin.ModelAdmin):
    search_fields = ['tipo_contrato']
    list_display = ('id', 'tipo_contrato')


@admin.register(ZonaBoleto)
class ZonaBoletoAdmin(admin.ModelAdmin):
    search_fields = ['zona']
    list_display = ('zona',)
