import requests
import folium
import numpy as np
import fastdtw
from math import sin, cos, sqrt, atan2, radians
import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import folium
from folium.plugins import MarkerCluster
from lineas.models import Recorrido


def score_desvio(recorrido, viaje, umbral_minimo=0):
    desvios_recorrido = np.array(desvios(recorrido, viaje))
    puntos_desviados = desvios_recorrido[desvios_recorrido > umbral_minimo]
    # puntos_desviados = puntos_desviados ** 2 # si quisieramos potenciar
    return puntos_desviados.sum() / len(recorrido)


def desvios(viaje, recorrido):
    # FUNCION PRINCIPAL
    # Dado un recorrido y un viaje, devuelve el desvío de cada punto del viaje con respecto al recorrido.
    # Utiliza DTW para detemrinar la asignación que produce la distancia mínima entre puntos.
    distancia, asignaciones = fastdtw.fastdtw(recorrido, viaje, dist=dist_mts, radius=len(recorrido))

    desvios_recorrido = np.full(len(recorrido), np.inf)
    for (i, j) in asignaciones:
        d = dist_mts(recorrido[i], viaje[j])
        desvios_recorrido[i] = min(desvios_recorrido[i], d)

    return desvios_recorrido


def dist_mts(x, y):
    R = 6373.0

    lat1 = radians(x[0])
    lon1 = radians(x[1])
    lat2 = radians(y[0])
    lon2 = radians(y[1])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c * 1000

    #     print("Result:", distance, "mts")
    return distance


def angulo(p1, p2):

    long_diff = np.radians(p2[1] - p1[1])

    lat1 = np.radians(p1[0])
    lat2 = np.radians(p2[0])

    x = np.sin(long_diff) * np.cos(lat2)
    y = (np.cos(lat1) * np.sin(lat2) - (np.sin(lat1) * np.cos(lat2) * np.cos(long_diff)))
    angulo_calculado = np.degrees(np.arctan2(x, y))

    # adjusting for compass angulo_calculado
    if angulo_calculado < 0:
        return angulo_calculado + 360
    return angulo_calculado


def dibujar_recorrido(recorrido, save_as=None, color="blue", mapit=None, line=False, arrows=False):
    if not mapit:
        mapit = folium.Map(location=(recorrido[len(recorrido) // 2][0],
                                     recorrido[len(recorrido) // 2][1]),
                           zoom_start=11)
    if line:
        folium.PolyLine(recorrido, color=color, weight=2.5, opacity=1).add_to(mapit)
    else:
        for lat, lng in recorrido:
            folium.Marker(location=[lat, lng], icon=folium.Icon(color=color)).add_to(mapit)

    if save_as:
        mapit.save(save_as + ".html")

    if arrows:
        for i, (r1, r2) in enumerate(zip(recorrido, recorrido[1:])):
            if i % 20 == 0:
                rotation = angulo(r1, r2) - 90

                folium.RegularPolygonMarker(location=r1,
                                            fill_color=color, number_of_sides=3,
                                            radius=10, rotation=rotation).add_to(mapit)

    return mapit


def dibujar_recorridos(recorridos, colors=None, line=True, arrows=False):
    m = None
    if not colors:
        colors = ["blue"] * len(recorridos)
    for i, r in enumerate(recorridos):
        m = dibujar_recorrido(r, mapit=m, color=colors[i], line=line, arrows=arrows)
    return m


def interpolar_entre(p1, p2, n_puntos=150):
    lats = np.linspace(start=p1[0], stop=p2[0], num=n_puntos)
    longs = np.linspace(start=p1[1], stop=p2[1], num=n_puntos)
    return list(zip(lats, longs))


def loc_to_lat_long(loc):
    from django.contrib.gis.geos import GEOSGeometry

    pnt = GEOSGeometry((loc))
    return pnt.coords


def desviar_metros(lat, long, desvio_mts_lat, desvio_mts_long):
    from math import pi, cos
    r_earth = 6378
    dx = desvio_mts_lat / 1000
    dy = desvio_mts_long / 1000
    new_latitude = lat + (dx / r_earth) * (180 / pi)
    new_longitude = long + (dy / r_earth) * (180 / pi) / cos(lat * pi / 180)
    return new_latitude, new_longitude


def movingaverage(values, window):
    weights = np.array([min(i, window - i) for i in range(0, window)])
    weights = weights / weights.sum()
    sma = np.convolve(values, weights, 'valid')
    return sma


def generar_n_desvios(recorrido, n, longitud_devio=6, ancho_desvio=100, ruido=30):
    # Longitud medida en puntos
    # Ancho medido en cuadras
    # ruido en metros.
    long_recorrido = len(recorrido)
    step_desvios = long_recorrido // (n + 1)
    desvios_mask = np.zeros(long_recorrido)
    for i in range(step_desvios, long_recorrido, step_desvios):
        desvios_mask[i - longitud_devio // 2:i + longitud_devio // 2] = ancho_desvio
    w = 10
    desvios_mask = movingaverage(desvios_mask, w)
    desvios_mask = np.pad(desvios_mask, (0, w - 1), mode="constant")

    desvios_mask += np.random.randint(-ruido, ruido, len(recorrido))

    recorrido_desviado = [desviar_metros(lat, lng, d, 0) for (lat, lng), d in zip(recorrido, desvios_mask)]
    return np.array(recorrido_desviado)


def cargar_recorridos_de_api():
    """ antes venia de un API GeoJSON, este es un intento a ojo, PROBAR """
    return [recorrido.trazado_extendido for recorrido in Recorrido.objects.filter(publicado=True, linea__publicado=True)]

def obtener_puntos_recorrido(id_recorrido, recorridos_invertidos):
    """ antes venia de un API GeoJSON, este es un intento a ojo, PROBAR """
    
    recorrido = Recorrido.objects.get(pk=id_recorrido)
    puntos = [(point.y, point.x) for point in recorrido.trazado_extendido]
    return np.array(puntos)
