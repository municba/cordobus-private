import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import folium

def trayecto_simplificado_celdas(grid_dataframe, trayecto):
    # Funcion que convierte una trayectoria en celdas sin repetidos consecutivos.
    return sacar_dups_consecutivos(trayecto_celdas(grid_dataframe, trayecto))

def validar_viaje(trayecto1_celdas, trayecto2_celdas):
    # Funcion que compara trayectos en formato celdas y calcula su distancia.
    return levenshtein_distance(trayecto1_celdas, trayecto2_celdas)


## Auxiliares:
def trayecto_celdas(grid_dataframe, trayecto):
    # convierte cada punto del trayecto en la celda correspondiente
    trayecto_celdas = []
    for p in trayecto:
        trayecto_celdas.append(celda(grid_dataframe, p))
    return trayecto_celdas

def celda(grid_dataframe, p):
    # Candidata a ser optimizada,
    # Busca en la grilla la lat y lng correspondientes.
    lat_cond = (p[0] > grid_dataframe.lower_left_lat) & (p[0] < grid_dataframe.upper_right_lat)
    lng_cond = (p[1] < grid_dataframe.lower_left_lng) & (p[1] > grid_dataframe.upper_right_lng)
    return grid_dataframe[lat_cond & lng_cond].name.values[0]

def sacar_dups_consecutivos(l):
    res = []
    last = None
    for e in l:
        if e != last:
            res.append(e)

        last = e
    return res


def obtener_grilla(type="dataframe"):
    if type not in ["dataframe", "json"]:
        raise Exception("formato incorrecto para grilla")

    # Devuelve la grilla para cordoba en formato json o formato dataframe de pandas.
    cordoba_top = (-31.261914, -64.346675)
    cordoba_bottom = (-31.521124, -64.013879)

    grid_json = get_geojson_grid(cordoba_top, cordoba_bottom, n=20) # Genera una grilla de 20x20 rectangulos.

    if type == "dataframe":
        grid_dataframe = []
        for box in grid_json:
            upper_right = box["properties"]["upper_right"]
            lower_left = box["properties"]["lower_left"]
            name = box["properties"]["name"]
            grid_dataframe.append(dict(upper_right_lat=upper_right[1], upper_right_lng=upper_right[0], lower_left_lat=lower_left[1], lower_left_lng=lower_left[0], name=name))
        grid_dataframe = pd.DataFrame(grid_dataframe)
        return grid_dataframe

    else:
        return grid_json

def get_geojson_grid(upper_right, lower_left, n=16):
    # SACADO DE https://www.jpytr.com/post/analysinggeographicdatawithfolium/
    all_boxes = []

    lat_steps = np.linspace(lower_left[0], upper_right[0], n + 1)
    lon_steps = np.linspace(lower_left[1], upper_right[1], n + 1)

    lat_stride = lat_steps[1] - lat_steps[0]
    lon_stride = lon_steps[1] - lon_steps[0]

    for i, lat in enumerate(lat_steps[:-1]):
        for j, lon in enumerate(lon_steps[:-1]):
            # Define dimensions of box in grid
            upper_left = [lon, lat + lat_stride]
            upper_right = [lon + lon_stride, lat + lat_stride]
            lower_right = [lon + lon_stride, lat]
            lower_left = [lon, lat]

            # Define json coordinates for polygon
            coordinates = [
                upper_left,
                upper_right,
                lower_right,
                lower_left,
                upper_left
            ]

            geo_json = {"type": "FeatureCollection",
                        "properties":{
                            "lower_left": lower_left,
                            "upper_right": upper_right,
                            "name": "c({},{})".format(n - i, n - j)
                        },
                        "features":[]}

            grid_feature = {
                "type":"Feature",
                "geometry":{
                    "type":"Polygon",
                    "coordinates": [coordinates],
                }
            }

            geo_json["features"].append(grid_feature)

            all_boxes.append(geo_json)

    return all_boxes



def dibujar_grilla(mapa, grid):

    for i, geo_json in enumerate(grid):

        color = plt.cm.Reds(i / len(grid))
        color = mpl.colors.to_hex(color)

        gj = folium.GeoJson(geo_json,
                            style_function=lambda feature, color=color: {
                                                                        'fillColor': color,
                                                                        'color':"black",
                                                                        'weight': 2,
                                                                        'dashArray': '5, 5',
                                                                        'fillOpacity': 0.15,
                                                                        })
        popup = folium.Popup(geo_json["properties"]["name"])
        gj.add_child(popup)

        mapa.add_child(gj)
    return grid



def levenshtein_distance(a, b):
    """Return the Levenshtein edit distance between two strings *a* and *b*."""
    if a == b:
        return 0
    if len(a) < len(b):
        a, b = b, a
    if not a:
        return len(b)
    previous_row = range(len(b) + 1)
    for i, column1 in enumerate(a):
        current_row = [i + 1]
        for j, column2 in enumerate(b):
            insertions = previous_row[j + 1] + 1
            deletions = current_row[j] + 1
            substitutions = previous_row[j] + (column1 != column2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    return previous_row[-1]
