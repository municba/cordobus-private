from django.db import models
from django.utils import timezone
import datetime
import pytz



class CortesPorHoraManager(models.Manager):

    # Cortes por hora de los ultimos 'dias'
    def ultimos_dias(self, dias=7):
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=dias)
        qs = self.filter(fecha__gte=desde)
        return qs


class CortesPorEmpresaManager(models.Manager):

    # Cortes por empresa en los ultimos 'dias'
    def ultimos_dias(self, dias=7):
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=dias)
        qs = self.filter(fecha__gte=desde)
        return qs


class CortesPorContratoManager(models.Manager):

    # Cortes por contrato en los ultimos 'dias'
    def ultimos_dias(self, dias=7):
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=dias)
        qs = self.filter(fecha__gte=desde)
        return qs


class CortesPorLineaManager(models.Manager):

    # Cortes por linea en los ultimos 'dias'
    def ultimos_dias(self, dias=7):
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=dias)
        qs = self.filter(fecha__gte=desde)
        return qs


class CortesPorInternoManager(models.Manager):

    # Cortes por zona en los ultimos 'dias'
    def ultimos_dias(self, dias=7):
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=dias)
        qs = self.filter(fecha__gte=desde)
        return qs


class CortesPorChoferManager(models.Manager):

    # Cortes por zona en los ultimos 'dias'
    def ultimos_dias(self, dias=7):
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=dias)
        qs = self.filter(fecha__gte=desde)
        return qs


class CortesPorZonaManager(models.Manager):

    # Cortes por zona en los ultimos 'dias'
    def ultimos_dias(self, dias=7):
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=dias)
        qs = self.filter(fecha__gte=desde)
        return qs
