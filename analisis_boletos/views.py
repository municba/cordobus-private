from .models import (
    CortesPorHora,
    CortesPorEmpresa,
    CortesPorContrato,
    CortesPorLinea,
    CortesPorInterno,
    CortesPorChofer,
    CortesPorZona
)
from django.shortcuts import render
from django.http import Http404
from django.db.models import Sum
from django.views.decorators.clickjacking import xframe_options_exempt
from django.contrib.auth.decorators import permission_required
from lineas.models import *
from django.utils import timezone
import django_excel as excel
import datetime
import pytz
from django.urls import reverse



def setear_dia(obj, dias=None):
    if dias is None:
        dias = 7
        result = obj.objects.ultimos_dias(dias=dias)
    else:
        dias = int(dias)
        result = obj.objects.ultimos_dias(dias=dias)
        if not result.exists():
            raise Http404("No hay datos de los ultimos {} dias".format(dias))

    return result


@xframe_options_exempt
def BoletosPorEmpresas(request, dias=None):

    cortes_por_empresa = setear_dia(obj=CortesPorEmpresa, dias=dias)
    data_final = []
    suma_boletos_total = 0
    # cantidad_dias = numero de dias a considerar.
    cantidad_dias = cortes_por_empresa.values('fecha').distinct().count()
    cortes_por_empresa = cortes_por_empresa.order_by('fecha')
    desde = cortes_por_empresa[0].fecha
    hasta = cortes_por_empresa[len(cortes_por_empresa)-1].fecha

    # <QuerySet [{'empresa': 'Coniferal S.A.C.I.F.', 'boletos': 344509}, {'empresa': 'ERSA Urbano S.A', 'boletos': 549915},
    # {'empresa': 'T.A.M. S.E. Trolebuses', 'boletos': 55985}, {'empresa': 'Autobuses Córdoba S.R.L', 'boletos': 86278}]>
    qs = cortes_por_empresa.values('empresa__nombre_publico').annotate(boletos=Sum('boletos_empresa')).order_by("-boletos")

    # Los id's deben quedar fijos en la db
    coniferal_obj = Empresa.objects.get(id=1)
    ersa_obj = Empresa.objects.get(id=2)
    aucor_obj = Empresa.objects.get(id=3)
    tamse_obj = Empresa.objects.get(id=4)

    for item in qs:
        data = []
        empresa = ""
        color = ""
        if item['empresa__nombre_publico'] == coniferal_obj.nombre_publico:
            empresa = coniferal_obj.nombre_publico
            color = coniferal_obj.color
        elif item['empresa__nombre_publico'] == ersa_obj.nombre_publico:
            empresa = ersa_obj.nombre_publico
            color = ersa_obj.color
        elif item['empresa__nombre_publico'] == tamse_obj.nombre_publico:
            empresa = tamse_obj.nombre_publico
            color = tamse_obj.color
        elif item['empresa__nombre_publico'] == aucor_obj.nombre_publico:
            empresa = aucor_obj.nombre_publico
            color = aucor_obj.color

        data.append(empresa)
        data.append(item['boletos'])
        data.append(str(item['boletos']))
        data.append(color)
        suma_boletos_total += item['boletos']
        data_final.append(data)

    context = {
                'lista_empresas': data_final,
                'cantidad_dias': cantidad_dias,
                'suma_boletos': suma_boletos_total,
                'desde': desde,
                'hasta': hasta,
                'titulo': 'GO - Boletos por empresa'
              }

    url = 'cortes_por_empresa.html'

    return render(request, url, context)


def BoletosPorEmpresasDiario(request):
    """
    Similar a la función de arriba, pero es abierta para que accedan personas
    sin loguearse.
    """

    # Los id's deben quedar fijos en la db
    coniferal_obj = Empresa.objects.get(id=1)
    ersa_obj = Empresa.objects.get(id=2)
    aucor_obj = Empresa.objects.get(id=3)
    tamse_obj = Empresa.objects.get(id=4)

    cortes_por_empresa_ayer = CortesPorEmpresa.objects.ultimos_dias(dias=1)
    if cortes_por_empresa_ayer.count() == 0:
        cortes_por_empresa_ayer = CortesPorEmpresa.objects.ultimos_dias(dias=2)

    suma_boletos_ayer = 0
    cortes_por_empresa_ayer = cortes_por_empresa_ayer.order_by('fecha')
    data_final_ayer = []

    if cortes_por_empresa_ayer.count() > 0:
        queryset = cortes_por_empresa_ayer.values('empresa__nombre_publico').annotate(boletos=Sum('boletos_empresa')).order_by("-boletos")

        for item in queryset:
            data = []
            empresa = ""
            color = ""
            if item['empresa__nombre_publico'] == coniferal_obj.nombre_publico:
                empresa = coniferal_obj.nombre_publico
                color = coniferal_obj.color
            elif item['empresa__nombre_publico'] == ersa_obj.nombre_publico:
                empresa = ersa_obj.nombre_publico
                color = ersa_obj.color
            elif item['empresa__nombre_publico'] == tamse_obj.nombre_publico:
                empresa = tamse_obj.nombre_publico
                color = tamse_obj.color
            elif item['empresa__nombre_publico'] == aucor_obj.nombre_publico:
                empresa = aucor_obj.nombre_publico
                color = aucor_obj.color

            data.append(empresa)
            data.append(item['boletos'])
            data.append(str(item['boletos']))
            data.append(color)
            suma_boletos_ayer += item['boletos']
            data_final_ayer.append(data)

        hoy = cortes_por_empresa_ayer[0].fecha

        descripcion_ayer = """En esta sección se puede ver la cantidad de pasajes
        cortados por cada una de las empresas del transporte urbano de
        pasajeros en el día de ayer.<br><br>
        <i><b>Nota importante:</b></i>  Los datos del día anterior se cargan automáticamente
        todos los días a las 11:00 hs. Por ende, en la franja horaria comprendida
        entre las 00:00 hs y las 11:00 hs, la visualización del día anterior
        estará desactualizada."""

    else:
        # No debería entrar nunca acá!!
        hoy = datetime.datetime.now()
        descripcion_ayer = """En estos momentos no tenemos los registros de cortes
                        de boletos del día de ayer.<br>
                        Nota importante: Los datos del día anterior se cargan
                        automáticamente todos los días a las 11:00 hs. Por ende,
                        en la franja horaria comprendida entre las 00:00 hs y las
                        11:00 hs, la visualización estará desactualizada
                        """

    # acumulado del mes
    dias = datetime.datetime.now()
    cortes_por_empresa = setear_dia(obj=CortesPorEmpresa, dias=dias.day - 1)
    data_final = []
    suma_boletos_total = 0
    # cantidad_dias = numero de dias a considerar.
    cantidad_dias = cortes_por_empresa.values('fecha').distinct().count()
    cortes_por_empresa = cortes_por_empresa.order_by('fecha')
    desde = cortes_por_empresa[0].fecha
    hasta = cortes_por_empresa[len(cortes_por_empresa)-1].fecha

    # <QuerySet [{'empresa': 'Coniferal S.A.C.I.F.', 'boletos': 344509}, {'empresa': 'ERSA Urbano S.A', 'boletos': 549915},
    # {'empresa': 'T.A.M. S.E. Trolebuses', 'boletos': 55985}, {'empresa': 'Autobuses Córdoba S.R.L', 'boletos': 86278}]>
    qs = cortes_por_empresa.values('empresa__nombre_publico').annotate(boletos=Sum('boletos_empresa')).order_by("-boletos")

    for item in qs:
        data = []
        empresa = ""
        color = ""
        if item['empresa__nombre_publico'] == coniferal_obj.nombre_publico:
            empresa = coniferal_obj.nombre_publico
            color = coniferal_obj.color
        elif item['empresa__nombre_publico'] == ersa_obj.nombre_publico:
            empresa = ersa_obj.nombre_publico
            color = ersa_obj.color
        elif item['empresa__nombre_publico'] == tamse_obj.nombre_publico:
            empresa = tamse_obj.nombre_publico
            color = tamse_obj.color
        elif item['empresa__nombre_publico'] == aucor_obj.nombre_publico:
            empresa = aucor_obj.nombre_publico
            color = aucor_obj.color

        data.append(empresa)
        data.append(item['boletos'])
        data.append(str(item['boletos']))
        data.append(color)
        suma_boletos_total += item['boletos']
        data_final.append(data)

    descripcion = """En esta sección se puede ver la cantidad de pasajes
        cortados por cada una de las empresas del transporte urbano de
        pasajeros en el mes en curso."""

    context = {
                'lista_empresas_ayer': data_final_ayer,
                'lista_empresas': data_final,
                'cantidad_dias': cantidad_dias,
                'suma_boletos_ayer': suma_boletos_ayer,
                'suma_boletos': suma_boletos_total,
                'desde': desde,
                'hasta': hasta,
                'titulo': 'Cantidad de boletos cortados por empresa',
                'descripcion': descripcion,
                'descripcion_ayer': descripcion_ayer,
                'fecha': hoy
              }

    url = 'cortes_por_empresa_diario.html'

    return render(request, url, context)


def BoletosPorEmpresasDescargable(request, mes, año, filetype):
    """
    Planilla de datos de boletos cortados por empresas en excel/csv
    """

    cortes_por_empresa = CortesPorEmpresa.objects.filter(fecha__month=mes,
                            fecha__year=año)
    if cortes_por_empresa.count() == 0:
        mensaje_error = ('No tenemos datos para el mes seleccionado.<br><br>'
            '<i><b>Nota:</b></i> Para filtrar por mes, debe ingresar el numero del '
            'mes correspondiente. Por ejemplo, para Enero es el número 1, '
            'Agosto se corresponde con el mes 8, etc.')
        return render(request, 'error_en_pagina.html',
            {'mensaje_error': mensaje_error})

    csv_list = []
    csv_list.append(['Empresa', 'Cantidad Boletos cortados', 'Fecha'])

    for cortes in cortes_por_empresa:
        csv_list.append([
            cortes.empresa,
            cortes.boletos_empresa,
            datetime.datetime.strftime(cortes.fecha, '%d/%m/%Y')
            ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@xframe_options_exempt
# @permission_required('analisis_boletos.ver_tableros_boletos')
def BoletosPorContrato(request, dias=None):

    cortes_por_contrato = setear_dia(obj=CortesPorContrato, dias=dias)

    data_final = []
    suma_boletos_total = 0
    # cantidad_dias = numero de dias a considerar.
    cantidad_dias = cortes_por_contrato.values('fecha').distinct().count()

    desde = cortes_por_contrato[0].fecha
    hasta = cortes_por_contrato[len(cortes_por_contrato)-1].fecha

    # <QuerySet [<CortesPorContrato: Fecha: 2019-04-07 - Contrato: Usuario común - Boletos: 120099>,
    # <CortesPorContrato: Fecha: 2019-04-07 - Contrato: Ord. 11876 c/a - Boletos: 6961>, ..., ...]
    qs = cortes_por_contrato.values('contrato__tipo_contrato').annotate(boletos=Sum('boletos_contrato'))
    qs_sin_comun = qs.exclude(contrato__tipo_contrato="Usuario común")
    qs_sin_comun = sorted(qs_sin_comun, key=lambda x: x['boletos'], reverse=True)

    for item in qs_sin_comun:
        data = []
        data.append(item['contrato__tipo_contrato'])
        data.append(item['boletos'])
        suma_boletos_total += item['boletos']
        data_final.append(data)

    context = {
                'lista_contrato': data_final,
                'cantidad_dias': cantidad_dias,
                'suma_boletos': suma_boletos_total,
                'desde': desde,
                'hasta': hasta
              }

    url = 'cortes_por_contrato.html'

    return render(request, url, context)


def BoletosPorContratoDescargable(request, mes, año, filetype):

    cortes_por_contrato = CortesPorContrato.objects.filter(fecha__month=mes,
                            fecha__year=año)
    if cortes_por_contrato.count() == 0:
        mensaje_error = ('No tenemos datos para el mes seleccionado.<br><br>'
            '<i><b>Nota:</b></i> Para filtrar por mes, debe ingresar el numero del '
            'mes correspondiente. Por ejemplo, para Enero es el número 1, '
            'Agosto se corresponde con el mes 8, etc.')
        return render(request, 'error_en_pagina.html',
            {'mensaje_error': mensaje_error})

    csv_list = []
    csv_list.append(['Fecha', 'Contrato', 'Cantidad Boletos Cortados'])

    for cortes in cortes_por_contrato:
        csv_list.append([
            datetime.datetime.strftime(cortes.fecha, '%d/%m/%Y'),
            cortes.contrato.tipo_contrato,
            cortes.boletos_contrato
            ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@xframe_options_exempt
# @permission_required('analisis_boletos.ver_tableros_boletos')
def BoletosConSubsidio(request, dias=None):

    cortes_por_contrato = setear_dia(obj=CortesPorContrato, dias=dias)

    # <QuerySet = cortes_por_contrato.values('contrato').annotate(boletos=Sum('boletos_contrato'))et [{'contrato': 'BEG DND 18u', 'boletos': 3}, {'contrato': 'BEG Inicial', 'boletos': 3},
    # {'contrato': 'BEG Primario', 'boletos': 213}, ..., ..., ..., ]>
    qs = cortes_por_contrato.values('contrato__tipo_contrato').annotate(boletos=Sum('boletos_contrato'))
    # Obtenemos el numero de usuarios comunes pero no lo mostramos en el grafico
    usuarios_comunes = qs.get(contrato__tipo_contrato="Usuario común")['boletos']
    # Excluimos usuarios comunes y de paso ordenamos los demas
    qs_sin_comun = qs.exclude(contrato__tipo_contrato='Usuario común').order_by("-boletos")
    # Cortes de boletos agrupados por BEG
    beg = qs_sin_comun.filter(contrato__tipo_contrato__icontains="BEG")
    beg_total = beg.aggregate(cant_boletos=Sum('boletos'))
    beg_total = beg_total['cant_boletos']
    # Cortes de boletos agrupados por BAM
    bam = qs_sin_comun.filter(contrato__tipo_contrato__icontains="BAM")
    bam_total = bam.aggregate(cant_boletos=Sum('boletos'))
    bam_total = bam_total['cant_boletos']
    # Cortes de boletos agrupados por BOS
    bos = qs_sin_comun.filter(contrato__tipo_contrato__icontains="BOS")
    bos_total = bos.aggregate(cant_boletos=Sum('boletos'))
    bos_total = bos_total['cant_boletos']
    # Cortes de boletos agrupados por ordananzas
    ordenanzas = qs_sin_comun.filter(contrato__tipo_contrato__icontains="Ord.")
    ordenanzas_total = ordenanzas.aggregate(cant_boletos=Sum('boletos'))
    ordenanzas_total = ordenanzas_total['cant_boletos']
    # Cortes de boletos agrupados por BSC
    bsc = qs_sin_comun.filter(contrato__tipo_contrato__icontains="BSC")
    bsc_total = bsc.aggregate(cant_boletos=Sum('boletos'))
    bsc_total = bsc_total['cant_boletos']

    data_final = []
    boletos_subsidio = 0
    # cantidad_dias = numero de dias a considerar
    cantidad_dias = cortes_por_contrato.values('fecha').distinct().count()
    cortes_por_contrato = cortes_por_contrato.order_by('fecha')
    desde = cortes_por_contrato[0].fecha
    hasta = cortes_por_contrato[len(cortes_por_contrato)-1].fecha

    # Pensar mejor las lineas de abajo
    contratos = [
                    ["BEG", beg_total, str(beg_total)],
                    ["BAM", bam_total, str(bam_total)],
                    ["BOS", bos_total, str(bos_total)],
                    ["BSC", bsc_total, str(bsc_total)],
                    ["Ordenanzas", ordenanzas_total, str(ordenanzas_total)]
                ]
    contratos = sorted(contratos, key=lambda x: x[1])

    boletos_subsidio = beg_total + bam_total + bos_total + ordenanzas_total + bsc_total

    l = []
    lista_usuarios_comunes = ["Usuarios sin subsidio", usuarios_comunes, str(usuarios_comunes)]
    l.append(lista_usuarios_comunes)
    context = {
                'lista_contrato': contratos,
                'cantidad_dias': cantidad_dias,
                'boletos_subsidio': boletos_subsidio,
                'usuarios_comunes': usuarios_comunes,
                'desde': desde,
                'hasta': hasta,
                'lista_usuarios_comunes': l
              }

    url = 'cortes_con_subsidio.html'

    return render(request, url, context)

def BoletosConSubsidioDiario(request):

    # cortes_por_contrato_ayer = setear_dia(obj=CortesPorContrato, dias=1)
    cortes_por_contrato_ayer = CortesPorContrato.objects.ultimos_dias(dias=1)
    if cortes_por_contrato_ayer.count() == 0:
        cortes_por_contrato_ayer = CortesPorContrato.objects.ultimos_dias(dias=2)

    boletos_subsidio = 0

    if cortes_por_contrato_ayer.count() > 0:
        # <QuerySet = cortes_por_contrato.values('contrato').annotate(boletos=Sum('boletos_contrato'))et [{'contrato': 'BEG DND 18u', 'boletos': 3}, {'contrato': 'BEG Inicial', 'boletos': 3},
        # {'contrato': 'BEG Primario', 'boletos': 213}, ..., ..., ..., ]>
        qs = cortes_por_contrato_ayer.values('contrato__tipo_contrato').annotate(boletos=Sum('boletos_contrato'))
        # Obtenemos el numero de usuarios comunes pero no lo mostramos en el grafico
        usuarios_comunes_ayer = qs.get(contrato__tipo_contrato="Usuario común")['boletos']
        # Excluimos usuarios comunes y de paso ordenamos los demas
        qs_sin_comun = qs.exclude(contrato__tipo_contrato='Usuario común').order_by("-boletos")
        # Cortes de boletos agrupados por BEG
        beg = qs_sin_comun.filter(contrato__tipo_contrato__icontains="BEG")
        beg_total = beg.aggregate(cant_boletos=Sum('boletos'))
        beg_total_ayer = beg_total['cant_boletos'] or 0

        # Cortes de boletos agrupados por BAM
        bam = qs_sin_comun.filter(contrato__tipo_contrato__icontains="BAM")
        bam_total = bam.aggregate(cant_boletos=Sum('boletos'))
        bam_total_ayer = bam_total['cant_boletos'] or 0

        # Cortes de boletos agrupados por BOS
        bos = qs_sin_comun.filter(contrato__tipo_contrato__icontains="BOS")
        bos_total = bos.aggregate(cant_boletos=Sum('boletos'))
        bos_total_ayer = bos_total['cant_boletos'] or 0

        # Cortes de boletos agrupados por ordananzas
        ordenanzas = qs_sin_comun.filter(contrato__tipo_contrato__icontains="Ord.")
        ordenanzas_total = ordenanzas.aggregate(cant_boletos=Sum('boletos'))
        ordenanzas_total_ayer = ordenanzas_total['cant_boletos'] or 0

        # Cortes de boletos agrupados por BSC
        bsc = qs_sin_comun.filter(contrato__tipo_contrato__icontains="BSC")
        bsc_total = bsc.aggregate(cant_boletos=Sum('boletos'))
        bsc_total_ayer = bsc_total['cant_boletos'] or 0

        # cantidad_dias = numero de dias a considerar
        cantidad_dias = cortes_por_contrato_ayer.values('fecha').distinct().count()
        cortes_por_contrato_ayer = cortes_por_contrato_ayer.order_by('fecha')

        descripcion_ayer = ('Boletos con subsidios discriminados por tipo de '
                            'viaje en el día de ayer. <br> <br>'
                            '<i><b>Nota importante:</b></i> Los datos del día anterior se '
                            'cargan automáticamente todos los días a las '
                            '11:00 hs. Por ende, en la franja horaria '
                            'comprendida entre las 00:00 y las 11:00 hs, '
                            'la visualización del día anterior estará desactualizada.')

        # Pensar mejor las lineas de abajo
        contratos = [
                        ["BEG: Boleto Educativo Gratuito", beg_total_ayer, str(beg_total_ayer)],
                        ["BAM: Boleto Adulto Mayores", bam_total_ayer, str(bam_total_ayer)],
                        ["BOS: Boleto Obrero Social", bos_total_ayer, str(bos_total_ayer)],
                        ["BSC: Boleto Social Cordobés", bsc_total_ayer, str(bsc_total_ayer)],
                        ["Ordenanzas: Boletos para discapacitados y acompañantes", ordenanzas_total_ayer, str(ordenanzas_total_ayer)]
                    ]

        contratos = sorted(contratos, key=lambda x: x[1])

        boletos_subsidio_ayer = (beg_total_ayer + bam_total_ayer + bos_total_ayer +
                            ordenanzas_total_ayer + bsc_total_ayer)

        l = []
        lista_usuarios_comunes_ayer = ["Usuarios sin subsidio", usuarios_comunes_ayer, str(usuarios_comunes_ayer)]
        l.append(lista_usuarios_comunes_ayer)

    else:
        #no debería entrar nunca acá!
        contratos = []
        boletos_subsidio_ayer = 0
        usuarios_comunes_ayer = 0
        l = ["Usuarios sin subsidio", 0, '0']
        descripcion_ayer = ('En estos momentos no tenemos los registros de '
            'cortes de boletos del día de ayer. <br><br>'
            '<i><b>Nota importante:</b></i> Los datos del día anterior se cargan '
            'automáticamente todos los días a las 11:00 hs. Por ende, en la '
            'franja horaria comprendida entre las 00:00 hs y las 11:00 hs, la '
            'visualización del día anterior estará desactualizada.')

    # total acumulado del mes
    dias = datetime.datetime.now()
    cortes_por_contrato = CortesPorContrato.objects.ultimos_dias(dias=dias.day - 1)
    qs = cortes_por_contrato.values('contrato__tipo_contrato').annotate(boletos=Sum('boletos_contrato'))
    # Obtenemos el numero de usuarios comunes pero no lo mostramos en el grafico
    usuarios_comunes = qs.get(contrato__tipo_contrato="Usuario común")['boletos']
    # Excluimos usuarios comunes y de paso ordenamos los demas
    qs_sin_comun = qs.exclude(contrato__tipo_contrato='Usuario común').order_by("-boletos")
    # Cortes de boletos agrupados por BEG
    beg = qs_sin_comun.filter(contrato__tipo_contrato__icontains="BEG")
    beg_total = beg.aggregate(cant_boletos=Sum('boletos'))
    beg_total = beg_total['cant_boletos'] or 0

    # Cortes de boletos agrupados por BAM
    bam = qs_sin_comun.filter(contrato__tipo_contrato__icontains="BAM")
    bam_total = bam.aggregate(cant_boletos=Sum('boletos'))
    bam_total = bam_total['cant_boletos'] or 0

    # Cortes de boletos agrupados por BOS
    bos = qs_sin_comun.filter(contrato__tipo_contrato__icontains="BOS")
    bos_total = bos.aggregate(cant_boletos=Sum('boletos'))
    bos_total = bos_total['cant_boletos'] or 0

    # Cortes de boletos agrupados por ordananzas
    ordenanzas = qs_sin_comun.filter(contrato__tipo_contrato__icontains="Ord.")
    ordenanzas_total = ordenanzas.aggregate(cant_boletos=Sum('boletos'))
    ordenanzas_total = ordenanzas_total['cant_boletos'] or 0

    # Cortes de boletos agrupados por BSC
    bsc = qs_sin_comun.filter(contrato__tipo_contrato__icontains="BSC")
    bsc_total = bsc.aggregate(cant_boletos=Sum('boletos'))
    bsc_total = bsc_total['cant_boletos'] or 0

    # cantidad_dias = numero de dias a considerar
    cantidad_dias = cortes_por_contrato.values('fecha').distinct().count()
    cortes_por_contrato = cortes_por_contrato.order_by('fecha')
    desde = cortes_por_contrato[0].fecha
    hasta = cortes_por_contrato[len(cortes_por_contrato)-1].fecha

    contratos_acumulado = [
                    ["BEG: Boleto Educativo Gratuito", beg_total, str(beg_total)],
                    ["BAM: Boleto Adulto Mayores", bam_total, str(bam_total)],
                    ["BOS: Boleto Obrero Social", bos_total, str(bos_total)],
                    ["BSC: Boleto Social Cordobés", bsc_total, str(bsc_total)],
                    ["Ordenanzas: Boletos para discapacitados y acompañantes", ordenanzas_total, str(ordenanzas_total)]
                ]

    contratos_acumulado = sorted(contratos_acumulado, key=lambda x: x[1])

    boletos_subsidio_acumulado = beg_total + bam_total + bos_total + ordenanzas_total + bsc_total

    acumulado = []
    lista_usuarios_comunes = ["Usuarios sin subsidio", usuarios_comunes, str(usuarios_comunes)]
    acumulado.append(lista_usuarios_comunes)
    descripcion = ('Boletos con subsidios discriminado por tipo de viaje para el mes en curso')

    context = {
                'titulo': 'Cortes de boletos discriminado por tipo de viaje',
                'descripcion_ayer': descripcion_ayer,
                'descripcion': descripcion,
                'lista_contrato': contratos,
                'lista_contrato_total': contratos_acumulado,
                'cantidad_dias': cantidad_dias,
                'boletos_subsidio': boletos_subsidio_ayer,
                'boletos_subsidio_acumulado': boletos_subsidio_acumulado,
                'usuarios_comunes_ayer': usuarios_comunes_ayer,
                'usuarios_comunes': usuarios_comunes,
                'desde': desde,
                'hasta': hasta,
                'lista_usuarios_comunes': l
              }

    url = 'cortes_con_subsidio_diario.html'

    return render(request, url, context)


@xframe_options_exempt
@permission_required('analisis_boletos.ver_tableros_boletos')
def BoletosSinSubsidio(request, dias=None):

    cortes_por_contrato = setear_dia(obj=CortesPorContrato, dias=dias)

    # <QuerySet = cortes_por_contrato.values('contrato').annotate(boletos=Sum('boletos_contrato'))et [{'contrato': 'BEG DND 18u', 'boletos': 3}, {'contrato': 'BEG Inicial', 'boletos': 3},
    # {'contrato': 'BEG Primario', 'boletos': 213}, ..., ..., ..., ]>
    qs = cortes_por_contrato.values('fecha').annotate(boletos=Sum('boletos_contrato'))

    boletos_sin_subsidio = 0
    # cantidad_dias = numero de dias a considerar
    cantidad_dias = cortes_por_contrato.values('fecha').distinct().count()
    data_final = []
    hasta = cortes_por_contrato[0].fecha
    desde = cortes_por_contrato[len(cortes_por_contrato)-1].fecha

    for item in qs:
        data = []
        data.append(item['cantidad_dias'])
        data.append(item['boletos'])
        boletos_sin_subsidio += item['boletos']
        data_final.append(data)

    context = {
                'cantidad_dias': cantidad_dias,
                'desde': desde,
                'hasta': hasta,
                'boletos_subsidio': boletos_sin_subsidio,
                'data_final': data_final
              }

    url = 'cortes_sin_subsidio.html'

    return render(request, url, context)


@xframe_options_exempt
def BoletosPorLinea(request, dias=None):

    cortes_por_linea = setear_dia(obj=CortesPorLinea, dias=dias)

    data_final = []
    suma_boletos_total = 0
    # cantidad_dias = numero de dias a considerar
    cantidad_dias = cortes_por_linea.values('fecha').distinct().count()
    # Ordenamos por fecha
    cortes_por_linea = cortes_por_linea.order_by('fecha')
    desde = cortes_por_linea[0].fecha
    hasta = cortes_por_linea[len(cortes_por_linea)-1].fecha

    # <QuerySet [{'linea__nombre_publico': '60', 'boletos': 121787}, {'linea__nombre_publico': '70', 'boletos': 102293},
    # {'linea__nombre_publico': '62', 'boletos': 89052}, ...., ....]
    qs = cortes_por_linea.values('linea__nombre_publico').annotate(boletos=Sum('boletos_linea')).order_by('-boletos')
    # Top 10
    qs = qs[:10]

    for item in qs:
        data = []
        linea = Linea.objects.get(nombre_publico=item['linea__nombre_publico'])
        color = linea.empresa.color
        data.append(item['linea__nombre_publico'])
        data.append(item['boletos'])
        data.append(str(item['boletos']))
        data.append(color)
        suma_boletos_total += item['boletos']
        data_final.append(data)

    context = {
                'lista_linea': data_final,
                'cantidad_dias': cantidad_dias,
                'suma_boletos': suma_boletos_total,
                'desde': desde,
                'hasta': hasta
              }

    url = 'cortes_top_10.html'

    return render(request, url, context)


def BoletosPorLineaConDescripcion(request):

    cortes_por_linea = CortesPorLinea.objects.ultimos_dias(dias=1)
    if cortes_por_linea.count() == 0:
        cortes_por_linea = CortesPorLinea.objects.ultimos_dias(dias=2)

    data_final = []
    suma_boletos_total = 0

    if cortes_por_linea.count() > 0:

        # Ordenamos por fecha
        cortes_por_linea = cortes_por_linea.order_by('fecha')

        # <QuerySet [{'linea__nombre_publico': '60', 'boletos': 121787}, {'linea__nombre_publico': '70', 'boletos': 102293},
        # {'linea__nombre_publico': '62', 'boletos': 89052}, ...., ....]
        qs = cortes_por_linea.values('linea__nombre_publico').annotate(boletos=Sum('boletos_linea')).order_by('-boletos')
        # Top 10
        qs = qs[:10]

        for item in qs:
            data = []
            linea = Linea.objects.get(nombre_publico=item['linea__nombre_publico'])
            color = linea.empresa.color
            data.append(item['linea__nombre_publico'])
            data.append(item['boletos'])
            data.append(str(item['boletos']))
            data.append(color)
            suma_boletos_total += item['boletos']
            data_final.append(data)

        hoy = cortes_por_linea[0].fecha
        descripcion = """El siguiente gráfico muestra la cantidad de cortes de
                boletos, realizados en el día de ayer, por las 10 líneas de
                colectivos con mayores cortes. <br><br>
                <i><b>Nota importante:</i></b> Los datos del día anterior se cargan
                automáticamente todos los días a las 11:00 hs. Por ende, en la
                franja horaria comprendida entre las 00:00 hs y las 11:00 hs, la
                visualización del día anterior estará desactualizada.
                """

    else:
        hoy = datetime.datetime.now()
        descripcion = """Todavía no tenemos los registros de cortes de boletos
                del día de ayer.<br><br>
                <i><b>Nota importante:</b></i> Los datos del día anterior se cargan
                automáticamente todos los días a las 11:00 hs. Por ende, en la
                franja horaria comprendida entre las 00:00 hs y las 11:00 hs la
                visualización del día anterior estará desactualizada.
                        """

    # acumulado del mes actual
    dias = datetime.datetime.now()

    cortes_por_linea_acumulado = setear_dia(obj=CortesPorLinea, dias=dias.day - 1)

    data_final_acumulada = []
    suma_boletos_total_acumulada = 0

    # Ordenamos por fecha
    cortes_por_linea_acumulado = cortes_por_linea_acumulado.order_by('fecha')

    desde = cortes_por_linea_acumulado[0].fecha
    hasta = cortes_por_linea_acumulado[len(cortes_por_linea_acumulado)-1].fecha

    queryset = cortes_por_linea_acumulado.values('linea__nombre_publico').annotate(boletos=Sum('boletos_linea')).order_by('-boletos')
    # Top 10
    queryset = queryset[:10]

    for item in queryset:
        data = []
        linea = Linea.objects.get(nombre_publico=item['linea__nombre_publico'])
        color = linea.empresa.color
        data.append(item['linea__nombre_publico'])
        data.append(item['boletos'])
        data.append(str(item['boletos']))
        data.append(color)
        suma_boletos_total_acumulada += item['boletos']
        data_final_acumulada.append(data)

    descripcion_del_acumulado = """El siguiente gráfico muestra la cantidad de cortes de
            boletos realizados en lo que va del mes actual, por las 10 líneas de
            colectivos con mayores cortes."""

    context = {
                'lista_linea': data_final,
                'suma_boletos': suma_boletos_total,
                'descripcion': descripcion,
                'fecha': hoy,
                'lista_linea_acumulada': data_final_acumulada,
                'suma_boletos_acumulada': suma_boletos_total_acumulada,
                'descripcion_acumulada': descripcion_del_acumulado,
                'desde': desde,
                'hasta': hasta
              }

    url = 'cortes_top_10_diario.html'

    return render(request, url, context)


def BoletosPorLineasDescargable(request, mes, año, filetype):
    """
    Planilla de datos de boletos cortados por lineas en excel/csv
    """

    cortes_por_linea = CortesPorLinea.objects.filter(fecha__month=mes,
                            fecha__year=año)
    if cortes_por_linea.count() == 0:
        mensaje_error = ('No tenemos datos para el mes seleccionado.<br><br>'
            '<i><b>Nota:</b></i> Para filtrar por mes, debe ingresar el numero del '
            'mes correspondiente. Por ejemplo, para Enero es el número 1, '
            'Agosto se corresponde con el mes 8, etc.')
        return render(request, 'error_en_pagina.html',
            {'mensaje_error': mensaje_error})

    csv_list = []
    csv_list.append(['Fecha', 'Linea', 'Cantidad Boletos Cortados'])

    for cortes in cortes_por_linea:
        csv_list.append([
            datetime.datetime.strftime(cortes.fecha, '%d/%m/%Y'),
            cortes.linea.nombre_publico,
            cortes.boletos_linea
            ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@xframe_options_exempt
# @permission_required('analisis_boletos.ver_tableros_boletos')
def BoletosPorHora(request, dias=None):

    cortes_por_hora = setear_dia(obj=CortesPorHora, dias=dias)

    data_final = []
    suma_boletos_total = 0
    # cantidad_dias = numero de dias a considerar
    cantidad_dias = cortes_por_hora.values('fecha').distinct().count()
    desde = cortes_por_hora[0].fecha
    hasta = cortes_por_hora[len(cortes_por_hora)-1].fecha

    # <QuerySet [{'hora': '1', 'boletos': 461},
    # {'hora': '9', 'boletos': 38251}, {'hora': '4', 'boletos': 583}, ..., ...]>
    qs = cortes_por_hora.values('hora').annotate(boletos=Sum('boletos'))

    for item in qs:
        data = []
        suma_boletos_total += item['boletos']
        data.append(int(item['hora']))
        data.append(item['boletos'])
        data.append(str(item['boletos']))
        data_final.append(data)

    data_final = sorted(data_final, key=lambda x: x[0])
    # Una vez ordenado volvemos a str el primer argumento para graficar
    for i in data_final:
        i[0] = str(i[0])

    context = {
                'lista_hora': data_final,
                'cantidad_dias': cantidad_dias,
                'suma_boletos': suma_boletos_total,
                'desde': desde,
                'hasta': hasta
              }

    url = 'cortes_por_hora.html'

    return render(request, url, context)


def BoletosPorHoraDescargable(request, mes, año, filetype):
    cortes_por_hora = CortesPorHora.objects.filter(fecha__month=mes,
                            fecha__year=año)
    if cortes_por_hora.count() == 0:
        mensaje_error = ('No tenemos datos para el mes seleccionado.<br><br>'
            '<i><b>Nota:</b></i> Para filtrar por mes, debe ingresar el numero del '
            'mes correspondiente. Por ejemplo, para Enero es el número 1, '
            'Agosto se corresponde con el mes 8, etc.')
        return render(request, 'error_en_pagina.html',
            {'mensaje_error': mensaje_error})

    csv_list = []
    csv_list.append(['Fecha', 'Hora', 'Cantidad boletos cortados'])

    for cortes in cortes_por_hora:
        csv_list.append([
            datetime.datetime.strftime(cortes.fecha, '%d/%m/%Y'),
            cortes.hora,
            cortes.boletos
            ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


def BoletosPorHoraDiario(request):

    cortes_por_hora = CortesPorHora.objects.ultimos_dias(dias=1)
    if cortes_por_hora.count() == 0:
        cortes_por_hora = CortesPorHora.objects.ultimos_dias(dias=2)

    data_final_ayer = []
    suma_boletos_total_ayer = 0
    cantidad_dias = 0
    hoy = datetime.datetime.now()

    if cortes_por_hora.count() > 0:

        # <QuerySet [{'hora': '1', 'boletos': 461},
        # {'hora': '9', 'boletos': 38251}, {'hora': '4', 'boletos': 583}, ..., ...]>
        qs = cortes_por_hora.values('hora').annotate(boletos=Sum('boletos'))

        for item in qs:
            data = []
            suma_boletos_total_ayer += item['boletos']
            data.append(int(item['hora']))
            data.append(item['boletos'])
            data.append(str(item['boletos']))
            data_final_ayer.append(data)

        data_final_ayer = sorted(data_final_ayer, key=lambda x: x[0])
        # Una vez ordenado volvemos a str el primer argumento para graficar
        for i in data_final_ayer:
            i[0] = str(i[0])

        # la variable hoy pasa a ser la fecha del último registro
        hoy = cortes_por_hora[0].fecha
        descripcion_ayer = ('Esta visualización muestra la cantidad de boletos'
            ' cortados en conjunto por todas las empresas prestadoras del '
            'servicio de transporte urbano de pasajeros por hora. <br><br>'
            '<i><b>Nota importante:</b></i> Los datos del día anterior se cargan '
            'automáticamente todos los días a las 11:00 hs. Por ende, en la '
            'franja horaria comprendida entre las 00:00 hs y las 11:00 hs, la '
            'visualización del día anterior estará desactualizada.')

    else:
        descripcion_ayer = ('No tenemos registro de los cortes de boletos del '
            'día de ayer. <br><br>'
            '<b><i>Nota importante:</i></b> Los datos del día anterior se cargan '
            'automáticamente todos los días a las 11:00 hs. Por ende, en la '
            'franja horaria comprendida entre las 00:00 hs y las 11:00 hs, la '
            'visualización del día anterior estará desactualizada.')

    cortes_por_hora_acumulado = CortesPorHora.objects.ultimos_dias(dias=hoy.day - 1)

    data_final_acumulada = []
    suma_boletos_total_acumulada = 0

    # cantidad_dias = numero de dias a considerar
    cantidad_dias = cortes_por_hora_acumulado.values('fecha').distinct().count()
    desde = cortes_por_hora_acumulado[0].fecha
    hasta = cortes_por_hora_acumulado[len(cortes_por_hora_acumulado)-1].fecha

    # <QuerySet [{'hora': '1', 'boletos': 461},
    # {'hora': '9', 'boletos': 38251}, {'hora': '4', 'boletos': 583}, ..., ...]>
    qs = cortes_por_hora_acumulado.values('hora').annotate(boletos=Sum('boletos'))

    for item in qs:
        data = []
        suma_boletos_total_acumulada += item['boletos']
        data.append(int(item['hora']))
        data.append(item['boletos'])
        data.append(str(item['boletos']))
        data_final_acumulada.append(data)

    data_final_acumulada = sorted(data_final_acumulada, key=lambda x: x[0])
    # Una vez ordenado volvemos a str el primer argumento para graficar
    for i in data_final_acumulada:
        i[0] = str(i[0])

    descripcion = ('Esta visualización muestra la cantidad de boletos'
        ' cortados en conjunto por todas las empresas prestadoras del '
        'servicio de transporte urbano de pasajeros en el mes en curso.')

    context = {
                'lista_hora': data_final_ayer,
                'cantidad_dias': cantidad_dias,
                'suma_boletos': suma_boletos_total_ayer,
                'descripcion_ayer': descripcion_ayer,
                'titulo': 'Cortes de boleto por rango horario',
                'fecha': hoy,
                'desde': desde,
                'hasta': hasta,
                'descripcion': descripcion,
                'lista_hora_acumulada': data_final_acumulada,
                'suma_boletos_acumulada': suma_boletos_total_acumulada
              }

    url = 'cortes_por_hora_diario.html'

    return render(request, url, context)

@xframe_options_exempt
@permission_required('analisis_boletos.ver_tableros_boletos')
def InternosRegistrados(request, dias=None):

    # Para obtener los ultimos 'dias' internos registrados
    internos_registrados = setear_dia(obj=CortesPorInterno, dias=dias)

    # cantidad_dias = numero de dias a considerar
    cantidad_dias = internos_registrados.values('fecha').distinct().count()
    desde = internos_registrados[0].fecha
    hasta = internos_registrados[len(internos_registrados)-1].fecha

    # <QuerySet [{'interno__empresa__nombre_publico': 'Coniferal', 'interno__numero': '130'},...,]
    # Contamos los internos (no duplicados) registrado los ultimos 'dias'
    qs_dis = internos_registrados.values('interno__numero', 'interno__empresa__nombre_publico').distinct()
    # Contamos los internos por empresa de los ultimos 'dias'
    internos_total = qs_dis.count()

    empresas = Empresa.objects.all()

    data_final = []
    for empresa in empresas:
        data = []
        data.append(empresa.nombre_publico)
        internos_empresa = qs_dis.filter(interno__empresa__nombre_publico=empresa.nombre_publico).count()
        data.append(internos_empresa)
        data.append(str(internos_empresa))
        data.append(empresa.color)
        data_final.append(data)

    # Ordenamos de mayor a menor la cantidad de internos
    data_final = sorted(data_final, key=lambda x: x[1], reverse=True)

    context = {
                'lista_internos': data_final,
                'cantidad_dias': cantidad_dias,
                'suma_boletos': internos_total,
                'desde': desde,
                'hasta': hasta
              }

    url = 'internos_registrados.html'

    return render(request, url, context)


def InternosRegistradosDescargable(request, mes, año, filetype):
    cortes_por_interno = CortesPorInterno.objects.filter(fecha__month=mes,
                            fecha__year=año)
    if cortes_por_interno.count() == 0:
        mensaje_error = ('No tenemos datos para el mes seleccionado.<br><br>'
            '<i><b>Nota:</b></i> Para filtrar por mes, debe ingresar el numero del '
            'mes correspondiente. Por ejemplo, para Enero es el número 1, '
            'Agosto se corresponde con el mes 8, etc.')
        return render(request, 'error_en_pagina.html',
            {'mensaje_error': mensaje_error})

    csv_list = []
    csv_list.append(['Fecha', 'Empresa', 'Interno', 'Cantidad Boletos cortados'])

    for cortes in cortes_por_interno:
        csv_list.append([
            datetime.datetime.strftime(cortes.fecha, '%d/%m/%Y'),
            cortes.interno.empresa.nombre_publico,
            cortes.interno.numero,
            cortes.boletos_interno
            ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@xframe_options_exempt
@permission_required('analisis_boletos.ver_tableros_boletos')
def ChoferesRegistrados(request, dias=None):

    # Para obtener los ultimos 'dias' choferes registrados
    choferes_registrados = setear_dia(obj=CortesPorChofer, dias=dias)

    # cantidad_dias = numero de dias a considerar
    cantidad_dias = choferes_registrados.values('fecha').distinct().count()
    desde = choferes_registrados[0].fecha
    hasta = choferes_registrados[len(choferes_registrados)-1].fecha

    # <QuerySet [{'chofer__empresa__nombre_publico': 'Coniferal', 'chofer__id_chofer_worldline': '1874'}, ...]
    # Contamos los choferes (no duplicados) registrado los ultimos 'dias'
    qs_dis = choferes_registrados.values('chofer__id_chofer_worldline', 'chofer__empresa__nombre_publico').distinct()
    # Contamos los choferes por empresa de los ultimos 'dias'
    internos_total = qs_dis.count()

    empresas = Empresa.objects.all()

    data_final = []
    for empresa in empresas:
        data = []
        data.append(empresa.nombre_publico)
        choferes_empresa = qs_dis.filter(chofer__empresa__nombre_publico=empresa.nombre_publico).count()
        data.append(choferes_empresa)
        data.append(str(choferes_empresa))
        data.append(empresa.color)
        data_final.append(data)

    # Ordenamos de mayor a menor la cantidad de choferes
    data_final = sorted(data_final, key=lambda x: x[1], reverse=True)

    context = {
                'lista_choferes': data_final,
                'cantidad_dias': cantidad_dias,
                'suma_boletos': internos_total,
                'desde': desde,
                'hasta': hasta
              }

    url = 'choferes_registrados.html'

    return render(request, url, context)


def CortesPorChoferDescargable(request, mes, año, filetype):
    cortes_por_chofer = CortesPorChofer.objects.filter(fecha__month=mes,
                            fecha__year=año)
    if cortes_por_chofer.count() == 0:
        mensaje_error = ('No tenemos datos para el mes seleccionado.<br><br>'
            '<i><b>Nota:</b></i> Para filtrar por mes, debe ingresar el numero del '
            'mes correspondiente. Por ejemplo, para Enero es el número 1, '
            'Agosto se corresponde con el mes 8, etc.')
        return render(request, 'error_en_pagina.html',
            {'mensaje_error': mensaje_error})

    csv_list = []
    csv_list.append(['Fecha', 'Id del chofer', 'Empresa', 'Cantidad boletos cortados'])

    for cortes in cortes_por_chofer:
        csv_list.append([
            datetime.datetime.strftime(cortes.fecha, '%d/%m/%Y'),
            cortes.chofer.id_chofer_worldline,
            cortes.chofer.empresa.nombre_publico,
            cortes.boletos_chofer
            ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)

def ChoferesRegistradosDiario(request):

    # Para obtener los ultimos 'dias' choferes registrados
    choferes_registrados_ayer = CortesPorChofer.objects.ultimos_dias(dias=1)
    if choferes_registrados_ayer.count() == 0:
        choferes_registrados_ayer = CortesPorChofer.objects.ultimos_dias(dias=2)

    data_final_ayer = []
    internos_total_ayer = 0
    hoy = datetime.datetime.now()
    empresas = Empresa.objects.all()

    if choferes_registrados_ayer.count() > 0:
        # cantidad_dias = numero de dias a considerar
        cantidad_dias = choferes_registrados_ayer.values('fecha').distinct().count()

        # <QuerySet [{'chofer__empresa__nombre_publico': 'Coniferal', 'chofer__id_chofer_worldline': '1874'}, ...]
        # Contamos los choferes (no duplicados) registrado los ultimos 'dias'
        qs_dis = choferes_registrados_ayer.values('chofer__id_chofer_worldline', 'chofer__empresa__nombre_publico').distinct()
        # Contamos los choferes por empresa de los ultimos 'dias'
        internos_total_ayer = qs_dis.count()

        for empresa in empresas:
            data = []
            data.append(empresa.nombre_publico)
            choferes_empresa = qs_dis.filter(chofer__empresa__nombre_publico=empresa.nombre_publico).count()
            data.append(choferes_empresa)
            data.append(str(choferes_empresa))
            data.append(empresa.color)
            data_final_ayer.append(data)

        # Ordenamos de mayor a menor la cantidad de choferes
        data_final_ayer = sorted(data_final_ayer, key=lambda x: x[1], reverse=True)

        descripcion_ayer = ('Esta visualización muestra la cantidad de '
            'choferes activos registrados por empresas en el día de ayer. Cada chofer '
            'que realice al menos un recorrido se registra y contabiliza en este gráfico. <br><br>'
            '<b><i>Nota importante:</i></b> Los datos del día anterior se cargan '
            'automáticamente todos los días a las 11:00 hs. Por ende, en la '
            'franja horaria comprendida entre las 00:00 hs y las 11:00 hs, la '
            'visualización del día anterior estará desactualizada.')
    else:
        descripcion_ayer = ('No tenemos registro de cortes de boletos del día de ayer.'
                        '<b><i>Nota importante:</i></b> Los datos del día anterior se cargan '
            'automáticamente todos los días a las 11:00 hs. Por ende, en la '
            'franja horaria comprendida entre las 00:00 hs y las 11:00 hs, la '
            'visualización del día anterior estará desactualizada.')


    choferes_registrados = CortesPorChofer.objects.ultimos_dias(dias=hoy.day - 1)

    # cantidad_dias = numero de dias a considerar
    cantidad_dias = choferes_registrados.values('fecha').distinct().count()
    desde = choferes_registrados[0].fecha
    hasta = choferes_registrados[len(choferes_registrados)-1].fecha

    # <QuerySet [{'chofer__empresa__nombre_publico': 'Coniferal', 'chofer__id_chofer_worldline': '1874'}, ...]
    # Contamos los choferes (no duplicados) registrado los ultimos 'dias'
    qs_dis = choferes_registrados.values('chofer__id_chofer_worldline', 'chofer__empresa__nombre_publico').distinct()
    # Contamos los choferes por empresa de los ultimos 'dias'
    internos_total = qs_dis.count()

    data_final = []
    for empresa in empresas:
        data = []
        data.append(empresa.nombre_publico)
        choferes_empresa = qs_dis.filter(chofer__empresa__nombre_publico=empresa.nombre_publico).count()
        data.append(choferes_empresa)
        data.append(str(choferes_empresa))
        data.append(empresa.color)
        data_final.append(data)

    # Ordenamos de mayor a menor la cantidad de choferes
    data_final = sorted(data_final, key=lambda x: x[1], reverse=True)
    descripcion = ('Esta visualización muestra la cantidad de '
            'choferes activos registrados por empresas en el mes en curso. Cada chofer '
            'que realice al menos un recorrido se registra y contabiliza en este gráfico.')

    fecha = hoy - datetime.timedelta(days=1)
    context = {
                'titulo': 'Cantidad de choferes registrados por empresa',
                'descripcion_ayer': descripcion_ayer,
                'descripcion': descripcion,
                'lista_choferes_ayer': data_final_ayer,
                'lista_choferes': data_final,
                'fecha': fecha.date,
                'cantidad_dias': cantidad_dias,
                'suma_boletos_ayer': internos_total_ayer,
                'suma_boletos': internos_total,
                'desde': desde,
                'hasta': hasta
              }

    url = 'choferes_registrados_diario.html'

    return render(request, url, context)


@xframe_options_exempt
@permission_required('analisis_boletos.ver_tableros_boletos')
def BoletosPorZona(request, dias=None):

    cortes_por_zona = setear_dia(obj=CortesPorZona, dias=dias)

    suma_boletos_total = 0
    data_final = []

    # cantidad_dias = numero de dias a considerar
    cantidad_dias = cortes_por_zona.values('fecha').distinct().count()
    desde = cortes_por_zona[0].fecha
    hasta = cortes_por_zona[len(cortes_por_zona)-1].fecha

    # <QuerySet [{'boletos': 3729, 'zona': 9}, {'boletos': 89376, 'zona': 7}, {'boletos': 126898, 'zona': 8}, ...]
    qs = cortes_por_zona.values('zona').annotate(boletos=Sum('boletos_zonas')).order_by('boletos')

    for item in qs:
        data = []
        suma_boletos_total += item['boletos']
        data.append(item['zona'])
        data.append(item['boletos'])
        data.append(str(item['boletos']))
        data_final.append(data)

    context = {
                'lista_zonas': data_final,
                'cantidad_dias': cantidad_dias,
                'suma_boletos': suma_boletos_total,
                'desde': desde,
                'hasta': hasta
              }

    url = 'cortes_por_zona.html'

    return render(request, url, context)


@xframe_options_exempt
@permission_required('analisis_boletos.ver_tableros_boletos')
def BoletosPorEmpresaServicioEspecial(request, dias=None):

    cortes_por_linea = setear_dia(obj=CortesPorLinea, dias=dias)

    data_final = []
    suma_boletos_total = 0
    # cantidad_dias = numero de dias a considerar
    cantidad_dias = cortes_por_linea.values('fecha').distinct().count()
    # Ordenamos por fecha
    cortes_por_linea = cortes_por_linea.order_by('fecha')
    desde = cortes_por_linea[0].fecha
    hasta = cortes_por_linea[len(cortes_por_linea)-1].fecha

    # Agrupamos por empresa
    qs_agrupado = cortes_por_linea.values('linea__empresa__nombre_publico')
    # Fitramos por servicio especial. '99' signifa servicio especial
    qs = qs_agrupado.filter(linea__nombre_publico__icontains='99').annotate(boletos=Sum('boletos_linea'))
    # Ordenamos por cortes de boletos
    qs = qs.order_by('-boletos')

    for item in qs:
        data = []
        empresa = Empresa.objects.get(nombre_publico=item['linea__empresa__nombre_publico'])
        color = empresa.color
        data.append(item['linea__empresa__nombre_publico'])
        data.append(item['boletos'])
        data.append(str(item['boletos']))
        data.append(color)
        suma_boletos_total += item['boletos']
        data_final.append(data)

    context = {
                'lista_linea': data_final,
                'cantidad_dias': cantidad_dias,
                'suma_boletos': suma_boletos_total,
                'desde': desde,
                'hasta': hasta
              }

    url = 'cortes_servicio_especial.html'

    return render(request, url, context)


def InformeSemanal(request, desde=None, hasta=None):
    """
    Informe semanal de sistema de transporte, aunque pasandole los
    parametros opcionales se puede cambiar las fechas a tomar.
    Nota importante: conviene generar el reporte despues de las 11am cuando
    el script baja los cortes y actualiza los datos
    Formato parametros opcional: dd-mm-yyyy
    """

    if (desde is None) and (hasta is None):
        # Si no esta definido lo seteamos siempre al dia de ayer
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=1)
        hasta = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=1)
    else:
        desde = datetime.datetime.strptime(desde, '%d-%m-%Y')
        hasta = datetime.datetime.strptime(hasta, '%d-%m-%Y')

    # ----- Cortes por empresa -----
    cortes_por_empresa_obj = CortesPorEmpresa.objects.filter(fecha__range=(desde, hasta))
    # Lista de diccionarios
    suma_cortes_por_empresa = cortes_por_empresa_obj.values('empresa__nombre_publico').annotate(cantidad_boletos=Sum('boletos_empresa'))
    suma_cortes_todas_empresas = suma_cortes_por_empresa.aggregate(total_boletos=Sum('cantidad_boletos'))['total_boletos']
    cantidad_dias = cortes_por_empresa_obj.values('fecha').distinct().count()

    # ----- Cortes por contrato agrupados -----
    cortes_por_contrato = CortesPorContrato.objects.filter(fecha__range=(desde, hasta))
    # <QuerySet = cortes_por_contrato.values('contrato').annotate(boletos=Sum('boletos_contrato'))et [{'contrato': 'BEG DND 18u', 'boletos': 3}, {'contrato': 'BEG Inicial', 'boletos': 3},
    # {'contrato': 'BEG Primario', 'boletos': 213}, ..., ..., ..., ]>

    # Obtenemos el numero de usuarios comunes
    usuarios_sin_subsidio = cortes_por_contrato.filter(contrato__tipo_contrato__icontains="Usuario común")
    suma_usuarios_sin_subsidio = usuarios_sin_subsidio.aggregate(cant_boletos=Sum('boletos_contrato'))['cant_boletos']

    # Cortes de boletos agrupados por BEG
    beg = cortes_por_contrato.filter(contrato__tipo_contrato__icontains="BEG")
    suma_beg = beg.values('fecha').aggregate(suma_beg=Sum('boletos_contrato'))['suma_beg']

    # Cortes de boletos agrupados por BAM
    bam = cortes_por_contrato.filter(contrato__tipo_contrato__icontains="BAM")
    suma_bam = bam.values('fecha').aggregate(suma_bam=Sum('boletos_contrato'))['suma_bam']

    # Cortes de boletos agrupados por BOS
    bos = cortes_por_contrato.filter(contrato__tipo_contrato__icontains="BOS")
    suma_bos = bos.values('fecha').aggregate(suma_bos=Sum('boletos_contrato'))['suma_bos']

    # Cortes de boletos agrupados por ordenanzas
    ordenanzas = cortes_por_contrato.filter(contrato__tipo_contrato__icontains="Ord.")
    suma_ordenanza = ordenanzas.values('fecha').aggregate(suma_ordenanza=Sum('boletos_contrato'))['suma_ordenanza']

    # Cortes de boletos agrupados por T.Social Mayores
    social_mayores = cortes_por_contrato.filter(contrato__tipo_contrato__icontains="T.Social")
    suma_social_mayores = social_mayores.values('fecha').aggregate(suma_social_mayores=Sum('boletos_contrato'))['suma_social_mayores']

    # Cortes de boletos agrupados por BSC
    bsc = cortes_por_contrato.filter(contrato__tipo_contrato__icontains="BSC")
    suma_bsc = bsc.values('fecha').aggregate(suma_bsc=Sum('boletos_contrato'))['suma_bsc']

    # Para mostrar la cantidad restante que falta
    otros = suma_cortes_todas_empresas - suma_usuarios_sin_subsidio - suma_beg - \
            suma_bam - suma_bos - suma_ordenanza - suma_social_mayores - suma_bsc

    # ----- Cortes top 10 lineas -----
    cortes_por_linea = CortesPorLinea.objects.filter(fecha__range=(desde, hasta))
    # <QuerySet [{'linea__nombre_publico': '60', 'boletos': 121787}, {'linea__nombre_publico': '70', 'boletos': 102293},
    # {'linea__nombre_publico': '62', 'boletos': 89052}, ...., ....]
    qs_linea = cortes_por_linea.values('linea__nombre_publico').annotate(boletos=Sum('boletos_linea')).order_by('fecha', '-boletos')
    # Top 10 de lineas mas usadas segun corte de boletos
    qs_linea = qs_linea[:10]
    qs_linea = list(qs_linea)

    # ----- Cortes horario pico -----
    cortes_por_hora = CortesPorHora.objects.filter(fecha__range=(desde, hasta))
    qs_hora = cortes_por_hora.values('hora').annotate(boletos_hora=Sum('boletos'))
    # Lo vamos a ordernar segun hora para luego mostrarlo
    for hora in qs_hora:
        hora['hora'] = int(hora['hora'])
    qs_hora_ordenada = sorted(qs_hora, key=lambda x: x['hora'])


    # ----- Cortes por internos -----
    # <QuerySet [{'interno__empresa__nombre_publico': 'Coniferal', 'interno__numero': '130'},...,]
    # Contamos los internos (no duplicados) registrado los ultimos 'dias'
    cortes_por_internos = CortesPorInterno.objects.filter(fecha__range=(desde, hasta))
    internos_total = cortes_por_internos.distinct().count()

    # ----- Cortes por choferes -----
    cortes_por_choferes = CortesPorChofer.objects.filter(fecha__range=(desde, hasta))
    choferes_total = cortes_por_choferes.distinct().count()

    # ----- Cortes servicio especiales -----
    # Agrupamos por empresa
    qs_agrupado = cortes_por_linea.values('linea__empresa__nombre_publico')
    # Fitramos por servicio especial. '99' signifa servicio especial
    qs_servicio_especial = qs_agrupado.filter(linea__nombre_publico__icontains='99').count()


    context = {
                'desde': desde,
                'hasta': hasta,
                'suma_cortes_por_empresa': suma_cortes_por_empresa,
                'suma_usuarios_sin_subsidio': suma_usuarios_sin_subsidio,
                'suma_beg': suma_beg,
                'suma_bam': suma_bam,
                'suma_bos': suma_bos,
                'suma_ordenanza': suma_ordenanza,
                'suma_social_mayores': suma_social_mayores,
                'suma_bsc': suma_bsc,
                'suma_cortes_todas_empresas': suma_cortes_todas_empresas,
                'otros': otros,
                'top_10_lineas': qs_linea,
                'qs_hora_ordenada': qs_hora_ordenada,
                'cantidad_colectivos': internos_total,
                'cantidad_choferes': choferes_total,
                'servicio_especial': qs_servicio_especial,
                'ahora': timezone.now(),
                'cantidad_dias': cantidad_dias
              }

    url = 'reporte_semanal.html'

    return render(request, url, context)


def AnalisisDescargables(request):
    """ Armamos una pagina para los csv's descargables """

    url = 'descargables.html'
    return render(request, url)
