from core.pagination import DefaultPagination
from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import (CortesPorHoraSerializer, CortesPorEmpresaSerializer, CortesPorContratoSerializer,
                        CortesPorLineaSerializer, CortesPorInternoSerializer, CortesPorChoferSerializer)
from analisis_boletos.models import (CortesPorHora, CortesPorEmpresa, CortesPorContrato,
                                    CortesPorLinea, CortesPorInterno, CortesPorChofer)



class CortesPorHoraViewSet(viewsets.ModelViewSet):
    """
    Cortes por hora. NOTA: van en rango de a una hora. Ej: hora: 2 va desde las 2am hasta las 2:59am
    Se puede filtrar por fecha con el parametro 'fecha'. El formato debe ser yyyy-mm-dd
    Ej: API/?fecha=2019-08-27
    """

    serializer_class = CortesPorHoraSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CortesPorHora.objects.all()

        fecha = self.request.query_params.get('fecha', None)
        if fecha is not None:
            queryset = queryset.filter(fecha=fecha)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CortesPorEmpresaViewSet(viewsets.ModelViewSet):
    """
    Cortes por empresa.
    Se puede filtrar por fecha con el parametro 'fecha'. El formato debe ser yyyy-mm-dd
    Ej: API/?fecha=2019-08-27
    Tambien se puede filtrar por empresa con su correspondiente id.
    Ej: API/?id_empresa=2
    """
    serializer_class = CortesPorEmpresaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CortesPorEmpresa.objects.all()

        fecha = self.request.query_params.get('fecha', None)
        if fecha is not None:
            queryset = queryset.filter(fecha=fecha)

        id_empresa = self.request.query_params.get('id_empresa', None)
        if id_empresa is not None:
            queryset = queryset.filter(empresa__id=id_empresa)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CortesPorContratoViewSet(viewsets.ModelViewSet):
    """
    Cortes por contrato.
    Se puede filtrar por el id del contrato.
    Ej: API/?id_contrato=2
    """
    serializer_class = CortesPorContratoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CortesPorContrato.objects.all()

        id_contrato = self.request.query_params.get('id_contrato', None)
        if id_contrato is not None:
            queryset = queryset.filter(contrato__id=id_contrato)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CortesPorLineaViewSet(viewsets.ModelViewSet):
    """
    Cortes por linea
    """
    serializer_class = CortesPorLineaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CortesPorLinea.objects.all()

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CortesPorInternoViewSet(viewsets.ModelViewSet):
    """
    Cortes por interno.
    Se puede filtrar por fecha con el parametro 'fecha'. El formato debe ser yyyy-mm-dd
    Ej: API/?fecha=2019-08-27
    """
    serializer_class = CortesPorInternoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # Filtramos por recorrido publicado
        queryset = CortesPorInterno.objects.all()

        fecha = self.request.query_params.get('fecha', None)
        if fecha is not None:
            queryset = queryset.filter(fecha=fecha)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CortesPorChoferViewSet(viewsets.ModelViewSet):
    """
    Cortes por chofer.
    Se puede filtrar por fecha con el parametro 'fecha'. El formato debe ser yyyy-mm-dd
    Ej: API/?fecha=2019-08-27
    """
    serializer_class = CortesPorChoferSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # Filtramos por recorrido publicado
        queryset = CortesPorChofer.objects.all()

        fecha = self.request.query_params.get('fecha', None)
        if fecha is not None:
            queryset = queryset.filter(fecha=fecha)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
