#!/usr/bin/python
from analisis_boletos.models import (CortesPorHora, CortesPorEmpresa, CortesPorContrato,
                                    CortesPorLinea, CortesPorInterno, CortesPorChofer, CortesPorZona)
from django.core.management.base import BaseCommand
from django.db import transaction
import csv
import sys
import pandas as pd
from lineas.models import Empresa, Linea
from boletos.models import Chofer, ContratoUsoBoleto, ZonaBoleto
from colectivos.models import Interno, OrigenDatoInterno



# TODO:
#Definir las diferentes partes del script en funciones e ir llamandolas en cadena.

class Command(BaseCommand):
    help = """ Comando para importar los csv de corte de boletos con su analisis """

    def add_arguments(self, parser):
        parser.add_argument('--path', help='Ruta al CSV con los datos a importar.')

    @transaction.atomic
    def handle(self, *args, **options):

        path = options['path']
        if path is None:
            self.stdout.write(self.style.ERROR('Debe especificar la ruta al archivo csv.'))
            sys.exit(1)

        self.stdout.write(self.style.SUCCESS('Importando csv desde {}'.format(path)))
        self.stdout.write(self.style.SUCCESS('Por favor, espere...'))

        # Levantamos el csv con los argumentos correspondientes al archivo
        df = pd.read_csv(path, encoding="ISO-8859-1", sep=";", parse_dates=True)

        #---------------------------------------------#

        # Creamos un df nuevo
        df_cortes_hora = pd.DataFrame()
        # A la columna CORTES BOLETOS le asigamos las fechas en datetime
        #df_cortes_hora['CORTES BOLETOS'] = pd.to_datetime(df['FECHA'], format='%d/%m/%Y %I:%M:%S %p')
        df_cortes_hora['CORTES BOLETOS'] = pd.to_datetime(df['FECHA'])
        fecha_fija = df_cortes_hora['CORTES BOLETOS'][2]
        fecha = fecha_fija.date()
        # Nuestro index va a ser la FECHA
        df_cortes_hora.index = df_cortes_hora['CORTES BOLETOS']
        # Agrupamos por hora y contamos
        df_cortes_hora = df_cortes_hora.resample('H').count()

        self.stdout.write(self.style.SUCCESS('Cargando datos de cortes por hora'))
        # Armamos los parametros para crear el objeto
        for i, k in enumerate(df_cortes_hora.index):
            hora = str(k.hour)
            try:
                cortes_hora, created = CortesPorHora.objects.get_or_create(fecha=fecha, hora=hora, boletos=df_cortes_hora['CORTES BOLETOS'][i])
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: {}'.format(e)))
                sys.exit(1)

        #---------------------------------------------#

        df_empresa = pd.DataFrame()
        df_empresa['EMPRESA'] = df['EMPRESA']
        df_empresa.index = df_empresa['EMPRESA']
        empresas = df_empresa['EMPRESA'].value_counts()

        self.stdout.write(self.style.SUCCESS('Cargando datos de cortes por empresa'))
        for i, empresa in enumerate(empresas.index):

            # Nos manejamos con strings porque NO llegan id's
            if empresa == "Coniferal S.A.C.I.F.":
                empresa = Empresa.objects.get(nombre_publico="Coniferal")
            elif empresa == "ERSA Urbano S.A":
                empresa = Empresa.objects.get(nombre_publico="ERSA")
            elif empresa == "T.A.M. S.E. Trolebuses":
                empresa = Empresa.objects.get(nombre_publico="TAMSE")
            elif empresa == "Autobuses Córdoba S.R.L":
                empresa = Empresa.objects.get(nombre_publico="Autobuses Córdoba")
            else:
                self.stdout.write(self.style.ERROR('Empresa "{}" mal escrita'.format(empresa)))
                sys.exit(1)

            try:
                cortes_empresa, created = CortesPorEmpresa.objects.get_or_create(fecha=fecha, empresa=empresa, boletos_empresa=empresas[i])
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: {}'.format(e)))
                sys.exit(1)

        #---------------------------------------------#

        df_contrato = pd.DataFrame()
        df_contrato['CONTRATO'] = df['CONTRATO']
        df_contrato.index = df_contrato['CONTRATO']
        contratos = df_contrato['CONTRATO'].value_counts()

        self.stdout.write(self.style.SUCCESS('Cargando datos de cortes por contrato'))
        for i, contrato in enumerate(contratos.index):
            contrato_obj, created = ContratoUsoBoleto.objects.get_or_create(tipo_contrato=contrato)
            try:
                cortes_contrato, created = CortesPorContrato.objects.get_or_create(fecha=fecha, contrato=contrato_obj, boletos_contrato=contratos[i])
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: {}'.format(e)))
                sys.exit(1)

        #---------------------------------------------#

        df_linea = pd.DataFrame()
        df_linea['LINEA'] = df['LINEA']
        df_linea.index = df_linea['LINEA']
        lineas = df_linea['LINEA'].value_counts()

        lineas_bien_escritas = ['A', 'B', 'C', 'B60', 'B61', 'B20', 'B30', 'B50', 'B70', 'B31']
        # servicio_especial = ['199', '299', '399', '499', '599', '699', '799', '899']

        self.stdout.write(self.style.SUCCESS('Cargando datos de cortes por lineas'))
        for i, linea in enumerate(lineas.index):
            # No queremos espacios invisibles!
            linea = linea.strip()
            # Las lineas vienen diferentes a como las tenemos. Y tambien sin id's
            if ('L' in linea):
                # L60 -> 60
                linea = linea[1:]
                # Registrando los internos que hicieron servicios especiales
                # if linea[1:] == "99":
                #     # Obtenemos el primer digito de la linea (Es lo unico que nos llega)
                #     linea_digito = linea[:1]
                #     # Ahora ya lo podemos asociar y sacar la empresa
                #     if linea_digito.startswith('1'):
            elif (linea == "AeroBus"):
                # AeroBus -> AEROBUS
                linea = linea.upper()
            elif (linea == "A601"):
                # A601 -> 601
                linea = linea[1:]
            elif (linea == "A600"):
                # A600 -> 600
                linea = linea[1:]
            # Si viene un trole lo dejamos pasar, vienen bien. (por ahora)
            elif (linea not in lineas_bien_escritas):
                self.stdout.write(self.style.ERROR('Linea "{}" mal escrita'.format(linea)))
                sys.exit(1)

            # Creamos el objeto
            try:
                linea_obj = Linea.objects.get(nombre_publico=linea)
            except Exception as e:
                # Linea mal escrita o no registrada en nuestra db
                self.stdout.write(self.style.ERROR('Linea "{}" no registrada'.format(linea)))
                sys.exit(1)

            try:
                cortes_lineas, created = CortesPorLinea.objects.get_or_create(fecha=fecha, linea=linea_obj, boletos_linea=lineas[i])
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: {}'.format(e)))
                sys.exit(1)

        #---------------------------------------------#

        df_interno = df[['COCHE', 'EMPRESA']]
        # Renombramos coche por interno
        df_interno.columns = ['INTERNO', 'EMPRESA']
        # Guardamos los internos sin duplicar.
        internos_sin_duplicar = df_interno.drop_duplicates()
        # Lo queremos en forma de lista de listas. Ej:
        # [[147, 'Coniferal S.A.C.I.F.'], [401, 'ERSA Urbano S.A']]
        internos_empresa = internos_sin_duplicar.to_numpy().tolist()

        # Tabla de internos. Ej de un registro:
        # |INTERNO| EMPRESA  |
        # |  146  | Coniferal|

        internos_coniferal = df_interno.loc[df_interno['EMPRESA']=='Coniferal S.A.C.I.F.']
        internos_ersa = df_interno.loc[df_interno['EMPRESA']=='ERSA Urbano S.A']
        internos_tamse = df_interno.loc[df_interno['EMPRESA']=='T.A.M. S.E. Trolebuses']
        internos_aucor = df_interno.loc[df_interno['EMPRESA']=='Autobuses Córdoba S.R.L']

        # Contando boletos por internos
        boletos_coniferal = internos_coniferal['INTERNO'].value_counts()
        boletos_ersa = internos_ersa['INTERNO'].value_counts()
        boletos_tamse = internos_tamse['INTERNO'].value_counts()
        boletos_aucor = internos_aucor['INTERNO'].value_counts()

        # Creamos las tuplas (interno, cantidad de boletos) de cada empresa
        interno_boleto_coniferal = [tuple((x, y)) for x, y in boletos_coniferal.items()]
        interno_boleto_ersa = [tuple((x, y)) for x, y in boletos_ersa.items()]
        interno_boleto_tamse = [tuple((x, y)) for x, y in boletos_tamse.items()]
        interno_boleto_aucor = [tuple((x, y)) for x, y in boletos_aucor.items()]

        self.stdout.write(self.style.SUCCESS('Cargando internos nuevos'))
        for lista in internos_empresa:
            # No me pasan id, asi que tengo que tomar los strings
            empresas = lista[1]
            interno = str(lista[0])
            if empresas == "Coniferal S.A.C.I.F.":
                empresa = Empresa.objects.get(nombre_publico="Coniferal")
            elif empresas == "ERSA Urbano S.A":
                empresa = Empresa.objects.get(nombre_publico="ERSA")
            elif empresas == "Autobuses Córdoba S.R.L":
                empresa = Empresa.objects.get(nombre_publico="Autobuses Córdoba")
            elif empresas == "T.A.M. S.E. Trolebuses":
                empresa = Empresa.objects.get(nombre_publico="TAMSE")
            else:
                self.stdout.write(self.style.ERROR('Empresa no reconocida {}'.format(empresas)))
                sys.exit(1)

            interno_obj, created = Interno.objects.get_or_create(numero=interno, empresa=empresa)
            if created:
                # Deberia quedar hardcodeado el id 2 a wordline
                origen_worldline = OrigenDatoInterno.objects.get(id=2)
                interno_obj.origen = origen_worldline
                interno_obj.save()

        self.stdout.write(self.style.SUCCESS('Cargando datos de cortes por internos'))

        # Grabamos Cortes por interno de la empresa coniferal
        for interno, boletos in interno_boleto_coniferal:
            interno = str(interno)
            empresa = Empresa.objects.get(nombre_publico="Coniferal")
            # Obtenemos los internos con la empresa que asociamos antes
            interno_obj = Interno.objects.get(numero=interno, empresa=empresa)
            # Grabamos datos de boleto en CortesPorInterno
            try:
                cortes_internos, created = CortesPorInterno.objects.get_or_create(fecha=fecha, interno=interno_obj, boletos_interno=boletos)
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: {}'.format(e)))
                sys.exit(1)

        # Grabamos Cortes por interno de la empresa ersa
        for interno, boletos in interno_boleto_ersa:
            interno = str(interno)
            empresa = Empresa.objects.get(nombre_publico="ERSA")
            # Obtenemos los internos con la empresa que asociamos antes
            interno_obj = Interno.objects.get(numero=interno, empresa=empresa)
            # Grabamos datos de boleto en CortesPorInterno
            try:
                cortes_internos, created = CortesPorInterno.objects.get_or_create(fecha=fecha, interno=interno_obj, boletos_interno=boletos)
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: {}'.format(e)))
                sys.exit(1)

        # Grabamos Cortes por interno de la empresa tamse
        for interno, boletos in interno_boleto_tamse:
            interno = str(interno)
            empresa = Empresa.objects.get(nombre_publico="TAMSE")
            # Obtenemos los internos con la empresa que asociamos antes
            interno_obj = Interno.objects.get(numero=interno, empresa=empresa)
            # Grabamos datos de boleto en CortesPorInterno
            try:
                cortes_internos, created = CortesPorInterno.objects.get_or_create(fecha=fecha, interno=interno_obj, boletos_interno=boletos)
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: {}'.format(e)))
                sys.exit(1)

        # Grabamos Cortes por interno de la empresa aucor
        for interno, boletos in interno_boleto_aucor:
            interno = str(interno)
            empresa = Empresa.objects.get(nombre_publico="Autobuses Córdoba")
            # Obtenemos los internos con la empresa que asociamos antes
            interno_obj = Interno.objects.get(numero=interno, empresa=empresa)
            # Grabamos datos de boleto en CortesPorInterno
            try:
                cortes_internos, created = CortesPorInterno.objects.get_or_create(fecha=fecha, interno=interno_obj, boletos_interno=boletos)
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: {}'.format(e)))
                sys.exit(1)


        #---------------------------------------------#

        # Creamos otro df para manejar las columnas de empresas y choferes
        df_choferes = pd.DataFrame()
        df_choferes['EMPRESA'] = df['EMPRESA']
        df_choferes['CHOFER'] = df['CHOFER']
        # Eliminamos choferes duplicados
        choferes_sin_duplicar = df_choferes.drop_duplicates()
        # Convertimos el df a lista de listas. Lista de chofer con la empresa a la que pertenece
        # Ej:[['Coniferal S.A.C.I.F.', 7188], ['ERSA Urbano S.A', 33014], ...]
        lista_choferes = choferes_sin_duplicar.to_numpy().tolist()


        # Tabla de choferes. Ej de un registro:
        # | EMPRESA | CHOFER |
        # |Coniferal|  7188  |

        choferes_coniferal = df_choferes.loc[df_choferes['EMPRESA']=='Coniferal S.A.C.I.F.']
        choferes_ersa = df_choferes.loc[df_choferes['EMPRESA']=='ERSA Urbano S.A']
        choferes_tamse = df_choferes.loc[df_choferes['EMPRESA']=='T.A.M. S.E. Trolebuses']
        choferes_aucor = df_choferes.loc[df_choferes['EMPRESA']=='Autobuses Córdoba S.R.L']

        # Contando boletos por choferes
        boletos_chofer_coniferal = choferes_coniferal['CHOFER'].value_counts()
        boletos_chofer_ersa = choferes_ersa['CHOFER'].value_counts()
        boletos_chofer_tamse = choferes_tamse['CHOFER'].value_counts()
        boletos_chofer_aucor = choferes_aucor['CHOFER'].value_counts()

        # Creamos las tuplas (id chofer, cantidad de boletos) de cada empresa
        chofer_boleto_coniferal = [tuple((x, y)) for x, y in boletos_chofer_coniferal.items()]
        chofer_boleto_ersa = [tuple((x, y)) for x, y in boletos_chofer_ersa.items()]
        chofer_boleto_tamse = [tuple((x, y)) for x, y in boletos_chofer_tamse.items()]
        chofer_boleto_aucor = [tuple((x, y)) for x, y in boletos_chofer_aucor.items()]


        self.stdout.write(self.style.SUCCESS('Cargando choferes nuevos'))
        for lista in lista_choferes:
            # No me pasan id, asi que tengo que tomar los strings
            empresas = lista[0]
            id_chofer = lista[1]
            if empresas == "Coniferal S.A.C.I.F.":
                empresa = Empresa.objects.get(nombre_publico="Coniferal")
            elif empresas == "ERSA Urbano S.A":
                empresa = Empresa.objects.get(nombre_publico="ERSA")
            elif empresas == "Autobuses Córdoba S.R.L":
                empresa = Empresa.objects.get(nombre_publico="Autobuses Córdoba")
            elif empresas == "T.A.M. S.E. Trolebuses":
                empresa = Empresa.objects.get(nombre_publico="TAMSE")
            else:
                self.stdout.write(self.style.ERROR('Empresa no reconocida {}'.format(empresas)))
                sys.exit(1)

            chofer_obj, created = Chofer.objects.get_or_create(id_chofer_worldline=id_chofer, empresa=empresa)


        self.stdout.write(self.style.SUCCESS('Cargando datos de choferes por empresa'))

        # Grabamos Cortes por chofer de la empresa coniferal
        for id_chofer, boletos in chofer_boleto_coniferal:
            id_chofer = str(id_chofer)
            empresa = Empresa.objects.get(nombre_publico="Coniferal")
            # Obtenemos los choferes con la empresa que asociamos antes
            chofer_obj = Chofer.objects.get(id_chofer_worldline=id_chofer, empresa=empresa)
            # Grabamos datos de boleto en CortesPorInterno
            try:
                cortes_chofer, created = CortesPorChofer.objects.get_or_create(fecha=fecha, chofer=chofer_obj, boletos_chofer=boletos)
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: {}'.format(e)))
                sys.exit(1)

        # Grabamos Cortes por chofer de la empresa ersa
        for id_chofer, boletos in chofer_boleto_ersa:
            id_chofer = str(id_chofer)
            empresa = Empresa.objects.get(nombre_publico="ERSA")
            # Obtenemos los choferes con la empresa que asociamos antes
            chofer_obj = Chofer.objects.get(id_chofer_worldline=id_chofer, empresa=empresa)
            # Grabamos datos de boleto en CortesPorInterno
            try:
                cortes_chofer, created = CortesPorChofer.objects.get_or_create(fecha=fecha, chofer=chofer_obj, boletos_chofer=boletos)
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: {}'.format(e)))
                sys.exit(1)

        # Grabamos Cortes por chofer de la empresa aucor
        for id_chofer, boletos in chofer_boleto_aucor:
            id_chofer = str(id_chofer)
            empresa = Empresa.objects.get(nombre_publico="Autobuses Córdoba")
            # Obtenemos los choferes con la empresa que asociamos antes
            chofer_obj = Chofer.objects.get(id_chofer_worldline=id_chofer, empresa=empresa)
            # Grabamos datos de boleto en CortesPorInterno
            try:
                cortes_chofer, created = CortesPorChofer.objects.get_or_create(fecha=fecha, chofer=chofer_obj, boletos_chofer=boletos)
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: {}'.format(e)))
                sys.exit(1)

        # Grabamos Cortes por chofer de la empresa tamse
        for id_chofer, boletos in chofer_boleto_tamse:
            id_chofer = str(id_chofer)
            empresa = Empresa.objects.get(nombre_publico="TAMSE")
            # Obtenemos los choferes con la empresa que asociamos antes
            chofer_obj = Chofer.objects.get(id_chofer_worldline=id_chofer, empresa=empresa)
            # Grabamos datos de boleto en CortesPorInterno
            try:
                cortes_chofer, created = CortesPorChofer.objects.get_or_create(fecha=fecha, chofer=chofer_obj, boletos_chofer=boletos)
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: {}'.format(e)))
                sys.exit(1)

        #---------------------------------------------#

        df_zona = pd.DataFrame()
        df_zona['ZONA'] = df['ZONA']
        zonas_unicas = df_zona.drop_duplicates()
        lista_zonas = zonas_unicas.to_numpy().tolist()
        zona_por_boleto = df_zona['ZONA'].value_counts()
        # Zonas con la cantidad de cortes asociado
        zona_boleto = [tuple((x, y)) for x, y in zona_por_boleto.items()]

        self.stdout.write(self.style.SUCCESS('Cargando zonas nuevas'))
        for zona in lista_zonas:
            zona_str = str(zona[0])
            zona_obj, created = ZonaBoleto.objects.get_or_create(zona=zona_str)


        self.stdout.write(self.style.SUCCESS('Cargando datos de cortes por zonas'))
        for zona, boletos in zona_boleto:
            # Obtenemos la zona que grabamos anteriormente
            zona_obj = ZonaBoleto.objects.get(zona=zona)
            try:
                cortes_zona, created = CortesPorZona.objects.get_or_create(fecha=fecha, zona=zona_obj, boletos_zonas=boletos)
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: {}'.format(e)))
                sys.exit(1)

        self.stdout.write(self.style.SUCCESS('Fin del script.'))
