from django.shortcuts import render, render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required
import datetime, locale
from colectivos.models import (EstacionAnioFrecuencias, DiasGrupoFrecuencias,
    FranjaHorariaFrecuencias, AgrupadorFranjaHoraria, Frecuencia)
from tracking.models import (InternoGPS, RegistroDeFrecuencia,
    RegistroDeDeslogueadoFrecuencia)
from lineas.models import Linea, Empresa
from colectivos.models import Interno
from analisis_boletos.models import CortesPorLinea
from tracking.tasks import calcular_frecuencias
from . import plots
import django_excel as excel
from django.template.defaulttags import register
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone
from django.views.decorators.cache import cache_page
from django.http import Http404


# filtro personalizado para usar en cualquier template
@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

@register.filter()
def to_int(value):
    return int(value)

def index (request):
    return render_to_response('index.html')

@cache_page(60 * 10)  # 10 min
@xframe_options_exempt
def estado_general(request):
    locale.setlocale(locale.LC_ALL, 'es_AR')
    ahora = datetime.datetime.now()
    dia_str = ahora.strftime('%A')
    hora = ahora.time()

    # FIXME: esto hay que corregir de alguna manera, en un futuro se agregarán estaciones
    estacion = EstacionAnioFrecuencias.objects.get(nombre='Normal')

    # se busca la franja horaria de ahora
    if dia_str == 'domingo':
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=3)
        # se busca el agrupador de franja horaria de este momento
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)
    elif dia_str == 'sabado':
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=2)
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)
    else:
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=1)
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)

    lineas = Linea.objects.exclude(nombre_publico='')
    lineas_nombres = [str(l.nombre_publico) for l in lineas if l.nombre_publico != '']
    internos = InternoGPS.objects.activos_ahora()
    no_logueados = internos.filter(Q(recorrido_actual__isnull=True) | Q(recorrido_actual__descripcion_corta=''))
    empresas = Empresa.objects.all()
    lista_no_logueados = no_logueados.values_list('name', flat=True)
    internos_registrados = Interno.objects.filter(numero__in=lista_no_logueados)

    ranking = {}
    deslogueados = {}
    for e in empresas:
        lineas_por_empresa = lineas.filter(empresa__nombre_publico=e.nombre_publico)
        frecuencia = Frecuencia.objects.filter(estacion=estacion,
                                        agrupador=franja_horaria.agrupador
                                        )

        ranking[e.nombre_publico] = {}
        for linea_nombre in lineas_por_empresa:
            try:
                f = frecuencia.get(linea__nombre_publico=linea_nombre.nombre_publico)
                ranking[e.nombre_publico][linea_nombre.nombre_publico] = (
                    internos.filter(recorrido_actual__linea__nombre_publico=linea_nombre.nombre_publico).count(),
                    f.calculo_unidades_activas_esperadas()
                    )
            except ObjectDoesNotExist:
                pass

        deslogueados[e.nombre_publico] = internos_registrados.filter(empresa__nombre_publico=e.nombre_publico).count()

    ranking_empresa = {}
    porcentaje_lineas = {}
    graficos = {}
    for k,v in ranking.items():
        # el valor será un contador
        ranking_empresa[k] = 0

        # el valor será el porcentaje de unidades activas
        porcentaje_lineas[k] = []

        script, div = plots.plot_frecuencia_por_hora(k)
        graficos[k] = (script, div)
        # itero sobre el dict de frecuencia
        for k1,v1 in v.items():
            # sólo sumo cuando la frecuencia es menor de la esperada
            ranking_empresa[k] += v1[1]-v1[0] if v1[1] > v1[0] else 0

            # si el porcentaje es 0, no lo agrego al resultado
            porc = (v1[0]/v1[1] if v1[0] != 0 else 0)*100
            if porc != 0:
                porcentaje_lineas[k].append((k1, (v1[0], v1[1], porc, v1[1]-v1[0])))

    # ordeno este dic por porcentaje, y sólo se pasa los 3 primeros
    porcentaje_lineas = {k:sorted(v, key=lambda porcentaje: porcentaje[1][3], reverse=True)[:3] for k,v in porcentaje_lineas.items()}

    context = {'colectivos_por_empresa': ranking_empresa,
                'porcentaje_por_lineas': porcentaje_lineas,
                'deslogueados': deslogueados,
                'graficos': graficos
                }

    return render(request, 'estado_general.html', context)

@cache_page(60 * 10)  # 10 min
@xframe_options_exempt
def estado_general_mobile(request):
    ranking_empresa, porcentaje_lineas, deslogueados = calcular_frecuencias(True)

    graficos = {}
    for e in Empresa.objects.all():
        script, div = plots.plot_frecuencia_por_hora(e.nombre_publico)
        graficos[e.nombre_publico] = (script, div)

    # deslogueados tiene una lista con los internos de cada empresa, acá se necesita la cantidad
    deslogueados = {k:len(v) for k,v in deslogueados.items()}
    context = {'colectivos_por_empresa': ranking_empresa,
                'porcentaje_por_lineas': porcentaje_lineas,
                'deslogueados': deslogueados,
                'graficos': graficos
                }

    return render(request, 'estado_general_mobile.html', context)


@cache_page(60 * 10)  # 10 min
@xframe_options_exempt
def prueba_template(request):

    context = {'titulo': 'Probando'}

    return render(request, 'template_tableros.html', context)


@cache_page(60 * 10)  # 10 min
@xframe_options_exempt
def estado_general_empresa(request, empresa):

    locale.setlocale(locale.LC_ALL, 'es_AR')
    ahora = datetime.datetime.now()
    dia_str = ahora.strftime('%A')
    hora = ahora.time()

    # FIXME: esto hay que corregir de alguna manera, en un futuro se agregarán estaciones
    estacion = EstacionAnioFrecuencias.objects.get(nombre='Normal')

    # se busca la franja horaria de ahora
    if dia_str == 'domingo':
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=3)
        # se busca el agrupador de franja horaria de este momento
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)
    elif dia_str == 'sabado':
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=2)
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)
    else:
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=1)
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)

    frecuencias = Frecuencia.objects.filter(estacion=estacion,
                                    agrupador=franja_horaria.agrupador
                                    )

    empresas = {'tamse': 4, 'aucor': 3, 'ersa': 2, 'coniferal': 1}

    empresa_obj = get_object_or_404(Empresa, pk=empresas[empresa])

    # los registros de frecuencia se guardan cada 5 min, solo necesito los últimos
    registros = RegistroDeFrecuencia.objects.filter(linea__empresa__nombre_publico=empresa_obj.nombre_publico,
        momento__timestamp__range=((ahora - datetime.timedelta(minutes=5)), ahora))

    # el queryset resultado debería contener un sólo elemento
    deslogueados = RegistroDeDeslogueadoFrecuencia.objects.filter(empresa__nombre_publico=empresa_obj.nombre_publico,
        momento__timestamp__range=((ahora - datetime.timedelta(minutes=5)), ahora))

    #en caso de no haber encontrado registro de deslogueados, retorna 0
    cantidad_deslogueados = deslogueados[0].deslogueados.count() if deslogueados.count() > 0 else 0

    ranking_lineas = {r.linea.nombre_publico:(r.colectivos_faltantes, r.cantidad_unidades_circulando,
                                                frecuencias.get(linea__nombre_publico=r.linea.nombre_publico).calculo_unidades_activas_esperadas()) for r in registros} # ORDENAR ESTA LISTA!
    ranking_ordenado = sorted(ranking_lineas.items(), key=lambda x: x[1][0], reverse=True)

    total_de_faltantes = [r.colectivos_faltantes for r in registros]

    script, div = plots.plot_frecuencia_por_hora(empresa_obj.nombre_publico, True, 'Hora', 'Unidades faltantes')

    # para probar localmente(tener cargadas las empresas de transporte igual a los datos de produccion):
    # (descomentar las siguientes lineas y comentar lo de arriba)

    # empresa_obj = Empresa.objects.get(pk=3)
    # cantidad_deslogueados = 5
    # ranking_ordenado = [('43', (5, 3, 8)), ('44', (4, 4, 8)), ('42', (1, 10, 11)), ('41', (1, 8, 9)), ('40', (0, 19, 14))]
    # total_de_faltantes = [4, 5, 1, 1, 0]
    # script = '\n<script type="text/javascript">\n  (function() {\n    var fn = function() {\n      Bokeh.safely(function() {\n        (function(root) {\n          function embed_document(root) {\n            \n          var docs_json = \'{"3e155029-214b-44d6-bc0f-a128a968cddd":{"roots":{"references":[{"attributes":{"source":{"id":"1040","type":"ColumnDataSource"}},"id":"1044","type":"CDSView"},{"attributes":{"callback":null,"data":{"x":[0,1,2,3,4,5,6,7,8,9,10,11,12,13],"y":[10.0,0.0,0.0,0.0,0.0,37.0,49.0,24.0,18.0,4.0,4.0,5.0,29.0,21.0]},"selected":{"id":"1057","type":"Selection"},"selection_policy":{"id":"1056","type":"UnionRenderers"}},"id":"1040","type":"ColumnDataSource"},{"attributes":{"callback":null,"renderers":[{"id":"1043","type":"GlyphRenderer"}],"tooltips":[["Hora","@x"],["Unidades faltantes","@y"]]},"id":"1045","type":"HoverTool"},{"attributes":{},"id":"1021","type":"WheelZoomTool"},{"attributes":{},"id":"1008","type":"LinearScale"},{"attributes":{"line_alpha":0.1,"line_color":"#1f77b4","line_width":3,"x":{"field":"x"},"y":{"field":"y"}},"id":"1037","type":"Line"},{"attributes":{"callback":null},"id":"1002","type":"DataRange1d"},{"attributes":{"dimension":1,"plot":{"id":"1001","subtype":"Figure","type":"Plot"},"ticker":{"id":"1016","type":"BasicTicker"}},"id":"1019","type":"Grid"},{"attributes":{},"id":"1006","type":"LinearScale"},{"attributes":{"plot":null,"text":""},"id":"1048","type":"Title"},{"attributes":{"axis_label":"Hora","formatter":{"id":"1050","type":"BasicTickFormatter"},"plot":{"id":"1001","subtype":"Figure","type":"Plot"},"ticker":{"id":"1011","type":"BasicTicker"}},"id":"1010","type":"LinearAxis"},{"attributes":{},"id":"1011","type":"BasicTicker"},{"attributes":{},"id":"1050","type":"BasicTickFormatter"},{"attributes":{"data_source":{"id":"1040","type":"ColumnDataSource"},"glyph":{"id":"1041","type":"Circle"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1042","type":"Circle"},"selection_glyph":null,"view":{"id":"1044","type":"CDSView"}},"id":"1043","type":"GlyphRenderer"},{"attributes":{},"id":"1052","type":"BasicTickFormatter"},{"attributes":{},"id":"1054","type":"UnionRenderers"},{"attributes":{"callback":null,"start":0},"id":"1004","type":"DataRange1d"},{"attributes":{},"id":"1055","type":"Selection"},{"attributes":{"line_color":"#1f77b4","line_width":3,"x":{"field":"x"},"y":{"field":"y"}},"id":"1036","type":"Line"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto","tools":[{"id":"1020","type":"PanTool"},{"id":"1021","type":"WheelZoomTool"},{"id":"1022","type":"BoxZoomTool"},{"id":"1023","type":"SaveTool"},{"id":"1024","type":"ResetTool"},{"id":"1025","type":"HelpTool"},{"id":"1045","type":"HoverTool"}]},"id":"1026","type":"Toolbar"},{"attributes":{},"id":"1056","type":"UnionRenderers"},{"attributes":{"bottom_units":"screen","fill_alpha":{"value":0.5},"fill_color":{"value":"lightgrey"},"left_units":"screen","level":"overlay","line_alpha":{"value":1.0},"line_color":{"value":"black"},"line_dash":[4,4],"line_width":{"value":2},"plot":null,"render_mode":"css","right_units":"screen","top_units":"screen"},"id":"1028","type":"BoxAnnotation"},{"attributes":{},"id":"1025","type":"HelpTool"},{"attributes":{"grid_line_color":{"value":null},"plot":{"id":"1001","subtype":"Figure","type":"Plot"},"ticker":{"id":"1011","type":"BasicTicker"}},"id":"1014","type":"Grid"},{"attributes":{"fill_alpha":{"value":0.1},"fill_color":{"value":"#1f77b4"},"line_alpha":{"value":0.1},"line_color":{"value":"#1f77b4"},"size":{"units":"screen","value":5},"x":{"field":"x"},"y":{"field":"y"}},"id":"1042","type":"Circle"},{"attributes":{},"id":"1057","type":"Selection"},{"attributes":{"axis_label":"Unidades faltantes","formatter":{"id":"1052","type":"BasicTickFormatter"},"plot":{"id":"1001","subtype":"Figure","type":"Plot"},"ticker":{"id":"1016","type":"BasicTicker"}},"id":"1015","type":"LinearAxis"},{"attributes":{"data_source":{"id":"1035","type":"ColumnDataSource"},"glyph":{"id":"1036","type":"Line"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1037","type":"Line"},"selection_glyph":null,"view":{"id":"1039","type":"CDSView"}},"id":"1038","type":"GlyphRenderer"},{"attributes":{"source":{"id":"1035","type":"ColumnDataSource"}},"id":"1039","type":"CDSView"},{"attributes":{"fill_color":{"value":"white"},"line_color":{"value":"#1f77b4"},"size":{"units":"screen","value":5},"x":{"field":"x"},"y":{"field":"y"}},"id":"1041","type":"Circle"},{"attributes":{"below":[{"id":"1010","type":"LinearAxis"}],"left":[{"id":"1015","type":"LinearAxis"}],"renderers":[{"id":"1010","type":"LinearAxis"},{"id":"1014","type":"Grid"},{"id":"1015","type":"LinearAxis"},{"id":"1019","type":"Grid"},{"id":"1028","type":"BoxAnnotation"},{"id":"1038","type":"GlyphRenderer"},{"id":"1043","type":"GlyphRenderer"}],"sizing_mode":"stretch_both","title":{"id":"1048","type":"Title"},"toolbar":{"id":"1026","type":"Toolbar"},"toolbar_location":null,"x_range":{"id":"1002","type":"DataRange1d"},"x_scale":{"id":"1006","type":"LinearScale"},"y_range":{"id":"1004","type":"DataRange1d"},"y_scale":{"id":"1008","type":"LinearScale"}},"id":"1001","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"1020","type":"PanTool"},{"attributes":{},"id":"1024","type":"ResetTool"},{"attributes":{},"id":"1023","type":"SaveTool"},{"attributes":{"callback":null,"data":{"x":[0,1,2,3,4,5,6,7,8,9,10,11,12,13],"y":[10.0,0.0,0.0,0.0,0.0,37.0,49.0,24.0,18.0,4.0,4.0,5.0,29.0,21.0]},"selected":{"id":"1055","type":"Selection"},"selection_policy":{"id":"1054","type":"UnionRenderers"}},"id":"1035","type":"ColumnDataSource"},{"attributes":{"overlay":{"id":"1028","type":"BoxAnnotation"}},"id":"1022","type":"BoxZoomTool"},{"attributes":{},"id":"1016","type":"BasicTicker"}],"root_ids":["1001"]},"title":"Bokeh Application","version":"1.0.4"}}\';\n          var render_items = [{"docid":"3e155029-214b-44d6-bc0f-a128a968cddd","roots":{"1001":"ccdd48b4-da3f-4be6-828a-a2f629ad0a93"}}];\n          root.Bokeh.embed.embed_items(docs_json, render_items);\n        \n          }\n          if (root.Bokeh !== undefined) {\n            embed_document(root);\n          } else {\n            var attempts = 0;\n            var timer = setInterval(function(root) {\n              if (root.Bokeh !== undefined) {\n                embed_document(root);\n                clearInterval(timer);\n              }\n              attempts++;\n              if (attempts > 100) {\n                console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing");\n                clearInterval(timer);\n              }\n            }, 10, root)\n          }\n        })(window);\n      });\n    };\n    if (document.readyState != "loading") fn();\n    else document.addEventListener("DOMContentLoaded", fn);\n  })();\n</script>'
    # div = '\n<div class="bk-root" id="ccdd48b4-da3f-4be6-828a-a2f629ad0a93" data-root-id="1001"></div>'

    context = {'lineas': ranking_ordenado[:3],
                'cantidad_deslogueados': cantidad_deslogueados,
                'plot': div,
                'script': script,
                'nombre_empresa': empresa_obj.nombre_publico,
                'faltantes': sum(total_de_faltantes)
    }

    return render(request, 'estado_general_empresa.html', context)

@xframe_options_exempt
def internos_por_linea(request):
    script, div = plots.internos_por_linea()

    context = {}
    context['plot'] = div if InternoGPS.objects.activos_ahora().count() != 0 else 'El sistema de recolección de datos no está funcionando en este momento'
    context['script'] = script

    return render(request, 'internos_por_linea.html', context)

@xframe_options_exempt
def internos_por_linea_mobile(request):
    script, div = plots.internos_por_linea()

    context = {}
    context['plot'] = div if InternoGPS.objects.activos_ahora().count() != 0 else 'El sistema de recolección de datos no está funcionando en este momento'
    context['script'] = script

    return render(request, 'internos_por_linea_mobile.html', context)

@login_required
def prueba_graficos_bokeh(request):
    """
    Template para probar distintos gráficos generados con bokeh
    """
    script, div = plots.plot_frecuencia_por_hora(empresa='ERSA')

    context = {}
    context['plot'] = div
    context['script'] = script

    return render(request, 'prueba_bokeh.html', context)

@login_required
def total_de_boletos_por_dia(request, filetype):
    '''
    Planilla de datos cortes de boletos diario en excel/csv
    '''

    qs = CortesPorLinea.objects.all().order_by('fecha')
    lineas = Linea.objects.all()

    for elem in qs:
        if ('L' in elem.linea or (elem.linea.startswith('A') and len(elem.linea) > 1 and elem.linea != 'AeroBus')):
            elem.linea = elem.linea[1:]

    csv_list = []
    csv_list.append(['fecha', 'linea', 'boletos cortados', 'color'])

    for q in qs:
        try:
            linea = lineas.get(nombre_publico=q.linea.upper())
        except Exception:
            continue
        csv_list.append([
            q.fecha,
            linea,
            q.boletos_linea,
            linea.empresa.color
            ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)

class InformeNoLogueadosView(LoginRequiredMixin, TemplateView):
    """
    Genera automaticamente un informe de los colectivos no logueados
    """

    template_name = "informe_no_logueados.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(InformeNoLogueadosView, self).get_context_data(**kwargs)

        flota_total = InternoGPS.objects.en_servicio().count()  # dieron servicio en los últimos 7 días
        internos_activos = InternoGPS.objects.activos_ahora()
        activos_no_logueados = internos_activos.filter(recorrido_actual__linea__nombre_publico='')
        internos_act_porc = round(internos_activos.count() / flota_total * 100, 2)

        #ordenar por empresa
        # internos_no_logueados = sorted(internos_no_logueados, key=lambda k: k['empresa'])

        context = {'internos_no_logueados': activos_no_logueados.order_by('empresa'), 'ahora': timezone.now(),
        'internos_activos': internos_activos.count(), 'flota_total': flota_total,
        'internos_act_porc': internos_act_porc}
        url = 'informe_tecnico.html'

        return context

class OrdenDeServicioView(LoginRequiredMixin, TemplateView):
    """
    Genera automaticamente una orden de servicio con el reclamo de los
    colectivos no logueados dirigido a una empresa.
    """

    template_name = "orden_de_servicio.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(OrdenDeServicioView, self).get_context_data(**kwargs)

        empresa = self.kwargs.get('empresa')
        # nombres oficiales de las empresas
        empresas = {'ersa': 'ERSA Urbano S.A.', 'aucor': 'Autobuses Córdoba',
        'tamse': 'TAMSE', 'coniferal': 'CONIFERAL S.A.C.I.F.'
        }
        activos_no_logueados = InternoGPS.objects.activos_ahora().filter(empresa__nombre_publico=empresas[empresa], recorrido_actual__linea__nombre_publico='')

        context = {'internos_no_logueados': activos_no_logueados, 'ahora': timezone.now(), 'empresa':empresas[empresa]}

        return context


@cache_page(60 * 60 * 24)  # 24 h
def ReporteEstadoGeneral(request, desde=None, hasta=None):
    """
    Generamos reporte diario (se puede cambiar) del estado en general de los bondis
    formato de fechas: dd-mm-yyyy
    """

    print(desde)
    print(type(desde))
    print(hasta)
    print(type(hasta))
    if (desde is None) and (hasta is None):
        # Si no esta definido lo seteamos siempre al dia de ayer
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=1)
        hasta = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=1)
    elif (desde == hasta):
        raise Http404("No se pueden obtener datos del mismo dia a la misma hora")
    else:
        desde = datetime.datetime.strptime(desde, '%d-%m-%Y')
        hasta = datetime.datetime.strptime(hasta, '%d-%m-%Y')

    # Obtenemos las empresas
    ersa = Empresa.objects.get(nombre_publico='ERSA')
    tamse = Empresa.objects.get(nombre_publico='TAMSE')
    aucor = Empresa.objects.get(nombre_publico='Autobuses Córdoba')
    coniferal = Empresa.objects.get(nombre_publico='Coniferal')
    # Obtenemos todos los registros filtrados 'desde' y 'hasta'
    registros = RegistroDeFrecuencia.objects.filter(momento__timestamp__range=(desde, hasta))
    # Contamos la cantidad de dias a considerar
    cantidad_dias = registros.values('momento__timestamp__date').distinct().count()
    # Filtramos los registros por empresas
    bondis_ersa = registros.filter(linea__empresa=ersa)
    bondis_tamse = registros.filter(linea__empresa=tamse)
    bondis_aucor = registros.filter(linea__empresa=aucor)
    bondis_coniferal = registros.filter(linea__empresa=coniferal)
    # Obtenemos los colectivos faltantes de cada empresa
    faltantes_ersa = []
    faltantes_tamse = []
    faltantes_aucor = []
    faltantes_coniferal = []

    [faltantes_ersa.append(bondi.colectivos_faltantes) for bondi in bondis_ersa]
    [faltantes_tamse.append(bondi.colectivos_faltantes) for bondi in bondis_tamse]
    [faltantes_aucor.append(bondi.colectivos_faltantes) for bondi in bondis_aucor]
    [faltantes_coniferal.append(bondi.colectivos_faltantes) for bondi in bondis_coniferal]
    # Sacamos los promedios
    promedio_faltantes_ersa = round(sum(faltantes_ersa)/len(faltantes_ersa), 2)
    promedio_faltantes_tamse = round(sum(faltantes_tamse)/len(faltantes_tamse), 2)
    promedio_faltantes_aucor = round(sum(faltantes_aucor)/len(faltantes_aucor), 2)
    promedio_faltantes_coniferal = round(sum(faltantes_coniferal)/len(faltantes_coniferal), 2)

    context = {
                'desde': desde,
                'hasta': hasta,
                'ersa': ersa.nombre_publico,
                'tamse': tamse.nombre_publico,
                'aucor': aucor.nombre_publico,
                'coniferal': coniferal.nombre_publico,
                'promedio_faltantes_ersa': promedio_faltantes_ersa,
                'promedio_faltantes_tamse': promedio_faltantes_tamse,
                'promedio_faltantes_aucor': promedio_faltantes_aucor,
                'promedio_faltantes_coniferal': promedio_faltantes_coniferal,
                'cantidad_dias': cantidad_dias
              }

    url = 'reporte_estado_general.html'

    return render(request, url, context)


@cache_page(60 * 10)  # 10 min
def ComprometidosVsReales(request):
    locale.setlocale(locale.LC_ALL, 'es_AR')
    ahora = datetime.datetime.now()
    dia_str = ahora.strftime('%A')
    hora = ahora.time()

    # FIXME: esto hay que corregir de alguna manera, en un futuro se agregarán estaciones
    estacion = EstacionAnioFrecuencias.objects.get(nombre='Normal')

    # se busca la franja horaria de ahora
    if dia_str == 'domingo':
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=3)
        # se busca el agrupador de franja horaria de este momento
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)
    elif dia_str == 'sabado':
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=2)
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)
    else:
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=1)
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)

    # Obtenemos todos lo internos (logueados y no logueados) activos en el ultimo minuto (bondis reales de hoy!)
    internos = InternoGPS.objects.activos_ahora(seconds=60)
    # Nos quedamos solo con los logueados
    internos_logueados = internos.exclude(name="").count()

    # Armo las fechas con el horario de la franja horaria correspondiente
    fecha_desde = datetime.datetime.combine(ahora.date(), franja_horaria.hora_desde)
    fecha_hasta = datetime.datetime.combine(ahora.date(), franja_horaria.hora_hasta)

    registros = RegistroDeFrecuencia.objects.filter(momento__timestamp__range=[fecha_desde, fecha_hasta])
    unidades_comprometidas = registros.distinct('linea_id')
    suma_unidades_comprometidas = 0
    for unidad_comprometida in unidades_comprometidas:
        suma_unidades_comprometidas += unidad_comprometida.unidades_esperadas

    # Activos sobre comprometidos
    porc_act_comp = internos_logueados/suma_unidades_comprometidas
    # No logueados sobre comprometidos
    porc_act_no_log = InternoGPS.objects.no_registrados().count()/suma_unidades_comprometidas
    # Adaptados sobre comprometidos
    porc_act_adap = InternoGPS.objects.adaptados_activos().count()/suma_unidades_comprometidas

    context = {
                'suma_unidades_comprometidas': suma_unidades_comprometidas,
                'suma_internos_logueados': internos_logueados,
                'porc_act_comp': porc_act_comp,
                'porc_act_no_log': porc_act_no_log,
                'porc_act_adap': porc_act_adap
                }

    return render(request, 'template_tableros.html', context)