import django_excel as excel
from django.contrib.auth.decorators import login_required
from colectivos.models import Interno
from django.views.decorators.cache import cache_page



@cache_page(60 * 60 * 12)  # 12 h
def ListaInternos(request, filetype):
    '''
    Lista para descargar los internos
    http://localhost:8000/colectivos/lista-de-internos.xls
    '''

    internos = Interno.objects.filter(activo=True).order_by('numero')
    csv_list = []
    csv_list.append(['Numero de interno', 'Empresa', 'Dominio', 'Modelo'])

    for interno in internos:

        csv_list.append([
            interno.numero,
            interno.empresa.nombre_publico,
            interno.dominio,
            interno.modelo,
            ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
