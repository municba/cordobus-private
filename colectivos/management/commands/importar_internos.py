#!/usr/bin/python
from colectivos.models import Interno, MarcaVehiculo, ModeloVehiculo
from lineas.models import Empresa
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils.text import slugify
import csv
import sys


class Command(BaseCommand):
    help = """Comando para importar datos de las unidades de transporte desde
                un archivo csv.
            """

    def add_arguments(self, parser):
        parser.add_argument('--path', help='Ruta al CSV con los datos a importar.')

    @transaction.atomic
    def handle(self, *args, **options):

        path = options['path']
        if path is None:
            self.stdout.write(self.style.ERROR('Debe especificar la ruta al archivo csv.'))
            sys.exit(1)

        self.stdout.write(self.style.SUCCESS('Importando csv desde {}'.format(path)))

        fieldnames = ['NUMERO', 'INTERNO', 'DOMINIO', 'AÑO', 'MARCA', 'MODELO', 'NUM_MOTOR', 'NUM_CHASIS', 'EMPRESA']

        count = 0
        unidades_nuevas = 0
        unidades_repetidas = 0

        motores_duplicados = 0
        chasis_duplicados = 0
        errores = []

        with open(path) as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames)

            header = reader.__next__()
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR('BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                print(header)
                print(fieldnames)
                sys.exit(1)

            for row in reader:
                count += 1
                # chequear los datos
                self.stdout.write(self.style.SUCCESS('{} Linea leida: {}'.format(count, row)))

                interno = slugify(row['INTERNO'].replace('_', '-'))
                empresa_str = row['EMPRESA'].strip()
                dominio = slugify(row['DOMINIO'].replace('_', '-'))
                modelo_str = row['MODELO'].strip()
                motor = row['NUM_MOTOR'].strip().replace(' ', '').replace('-', '').upper()
                chasis = row['NUM_CHASIS'].strip().replace(' ', '').replace('-', '').upper()
                anio = row['AÑO'].strip() or None
                marca_str = row['MARCA'].strip()
                obs = ''

                try:
                    unidad, created = Interno.objects.get_or_create(dominio=dominio,
                                            numero=interno)
                except Exception as e:
                    self.stdout.write(self.style.ERROR(e))
                    errores.append('Error {}. Dominio: {} Interno: {}'.format(e, dominio, interno))
                    # controlamos que error se da para anotarlo en observaciones
                    dominio_repetido = Interno.objects.filter(dominio=dominio).count() > 0
                    interno_repetido = Interno.objects.filter(numero=interno).count() > 0

                    # de todas formas se crea el interno para que lo corrija el encargado de estos datos
                    unidad = Interno.objects.create(dominio=dominio+'REPETIDO',
                                    numero=interno+'REPETIDO')

                    obs = ''
                    if dominio_repetido:
                        obs += 'Dominio repetido.'

                    if interno_repetido:
                        obs += 'Interno repetido.'

                    unidad.observaciones = obs
                    unidad.hay_que_revisar = obs != ''
                    unidad.save()

                if created:
                    unidades_nuevas += 1
                else:
                    unidades_repetidas += 1

                if empresa_str in ['AUTOBUSES CORDOBA S.R.L.']:
                    empresa = Empresa.objects.get(nombre_publico='Autobuses Córdoba')
                elif empresa_str in ['TAMSE']:
                    empresa = Empresa.objects.get(nombre_publico='TAMSE')
                elif empresa_str in ['ERSA', 'ERSA URBANO S.A.']:
                    empresa = Empresa.objects.get(nombre_publico='ERSA')
                elif empresa_str in ['CONIFERAL']:
                    empresa = Empresa.objects.get(nombre_publico='Coniferal')
                else:
                    self.stdout.write(self.style.ERROR('Empresa no reconocida {}'.format(empresa_str)))
                    sys.exit(1)

                # evitar duplicados (la base es muy mala)
                unidades_con_motor_igual = Interno.objects.filter(motor_numero=motor).exclude(id=unidad.id)
                existe_motor = len(unidades_con_motor_igual) > 0

                if existe_motor:
                    u = unidades_con_motor_igual[0]
                    obs += 'Número de MOTOR duplicado con {} (int {})'.format(u.dominio, u.numero)
                    motores_duplicados += 1
                    motor = '{}-ERROR{}'.format(motor, motores_duplicados)

                unidades_con_chasis_igual = Interno.objects.filter(chasis_numero=chasis).exclude(id=unidad.id)
                existe_chasis = len(unidades_con_chasis_igual) > 0

                if existe_chasis:
                    u = unidades_con_chasis_igual[0]
                    obs += 'Número de CHASIS duplicado con {} (int {})'.format(u.dominio, u.numero)
                    chasis_duplicados += 1
                    chasis = '{}-ERROR{}'.format(chasis, chasis_duplicados)

                hay_que_revisar = obs != ''
                unidad.observaciones = obs if hay_que_revisar is True else ''
                unidad.empresa = empresa
                unidad.chasis_numero = chasis
                unidad.motor_numero = motor
                unidad.modelo_anio = anio

                marca, marca_created = MarcaVehiculo.objects.get_or_create(nombre=marca_str)
                modelo, modelo_created = ModeloVehiculo.objects.get_or_create(marca=marca, nombre=modelo_str)

                unidad.modelo = modelo
                unidad.hay_que_revisar = hay_que_revisar
                unidad.save()
                self.stdout.write(self.style.SUCCESS('Unidad nueva DOM {} INT {}'.format(dominio, interno)))

        if len(errores) > 0:
            self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR INTERNOS:{}'.format(len(errores))))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))

        self.stdout.write(self.style.SUCCESS('Archivo cargado con éxito, se procesaron {} registros'.format(count)))
        self.stdout.write(self.style.SUCCESS('{} vehiculos nuevos, {} repetidos'.format(unidades_nuevas, unidades_repetidas)))
        self.stdout.write(self.style.SUCCESS('{} motores repetidos, {} chasis repetidos'.format(motores_duplicados, chasis_duplicados)))