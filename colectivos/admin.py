from django.contrib import admin
from .models import (MarcaVehiculo, ModeloVehiculo, TipoCombustible, Interno,
    Frecuencia, DiasGrupoFrecuencias, AgrupadorFranjaHoraria,
    FranjaHorariaFrecuencias, EstacionAnioFrecuencias, OrigenDatoInterno)


@admin.register(MarcaVehiculo)
class MarcaVehiculoAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    list_display = ('id', 'nombre')


@admin.register(ModeloVehiculo)
class ModeloVehiculoAdmin(admin.ModelAdmin):
    search_fields = ['marca__nombre', 'nombre']
    list_display = ('id', 'marca', 'nombre')
    list_filter = ['marca']


@admin.register(TipoCombustible)
class TipoCombustibleAdmin(admin.ModelAdmin):
    search_fields = ['nombre', 'id']
    list_display = ('id', 'nombre')


@admin.register(Interno)
class InternoAdmin(admin.ModelAdmin):
    list_display = ['numero', 'dominio', 'modelo_anio', 'activo', 'chasis_marca',
                    'chasis_numero', 'motor_numero', 'tanque_litros', 'asientos',
                    'hay_que_revisar', 'empresa', 'observaciones', 'fecha_alta', 'fecha_baja']
    list_filter = ['empresa', 'hay_que_revisar', 'activo']
    search_fields = ['numero', 'dominio', 'chasis_marca',
                    'chasis_numero', 'motor_numero']


@admin.register(Frecuencia)
class FrecuenciaAdmin(admin.ModelAdmin):

    def unidades_activas_esperadas(self, obj):
            return obj.calculo_unidades_activas_esperadas()

    list_editable = ('frecuencia_en_minutos_esperada',)
    list_display = ['estacion', 'agrupador', 'linea', 'frecuencia_en_minutos_esperada',
        'unidades_activas_esperadas', 'descripcion']
    search_fields = ['descripcion']
    list_filter = ['estacion', 'agrupador', 'linea__nombre_publico']


@admin.register(DiasGrupoFrecuencias)
class DiasGrupoFrecuenciasAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'lunes', 'martes', 'miercoles', 'jueves',
        'viernes', 'sabado', 'domingo', 'feriado']


@admin.register(AgrupadorFranjaHoraria)
class AgrupadorFranjaHorariaAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'dias']
    list_filter = ['nombre', 'dias']


@admin.register(FranjaHorariaFrecuencias)
class FranjaHorariaFrecuenciasAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'hora_desde', 'hora_hasta', 'agrupador']
    list_filter = ['nombre', 'agrupador']


admin.site.register(OrigenDatoInterno)

admin.site.register(EstacionAnioFrecuencias)
