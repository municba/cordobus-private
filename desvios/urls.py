from django.conf.urls import url

from . import views

urlpatterns = [
    # desvios
    url(r'^$', views.DesviosPorLineaView.as_view(), name='desvios-colectivos'),
]
