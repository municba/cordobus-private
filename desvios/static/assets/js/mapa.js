$( document ).ready(function() {

    window.getMapInfo= _getMapInfo;
    window.setMapInfo= _setMapInfo;
    window.reloadMapInfo = _reloadMapInfo;
    window.toggleMapInfo = _toggleMapInfo;
    window.setUmbral= _setUmbral;
    window.refeshMap = _refeshMap;

    window.addLayerMap = _addLayerMap;
    window.getZIndexByLayerName = _getZIndexByLayerName;
    window.setLayerObject = _setLayerObject;

    var color_trayectoria = '#00AA22';
    
    var color_relleno_puntos_gps_colectivo = '#5db8e2';
    var color_borde_puntos_gps_colectivo = '#0000ff';

    var color_relleno_desvios_colectivo = '#ff0000';
    var color_borde_desvios_colectivo = '#f442d4';

    var color_relleno_recorridook_colectivo = '#00AA22';
    var color_borde_recorridook_colectivo = '#22844e';

    $('#recorridos_ok_ref')[0].style.fill = color_relleno_recorridook_colectivo;
    $('#recorridos_ok_ref')[0].style.stroke = color_borde_recorridook_colectivo;

    $('#recorridos_mal_ref')[0].style.fill = color_relleno_desvios_colectivo;
    $('#recorridos_mal_ref')[0].style.stroke = color_borde_desvios_colectivo;

    var _mapInfo = {
        'indice_validacion':undefined,
        'media_desvios': undefined,
        'punto_recorrido':undefined
    }

     /**
    * Inicio de definicion de estilos para el mapa
    **/
    trayectoriaStyle = new ol.style.Style(
        {
        stroke: new ol.style.Stroke({color: color_trayectoria, width: 2})
        });

    var fill = new ol.style.Fill({color: color_relleno_puntos_gps_colectivo});

    pointStyle = new ol.style.Style(
        {
        image: new ol.style.Circle( /** @type {olx.style.IconOptions} */ ({
            radius: 4,
            fill: new ol.style.Fill({color: color_relleno_puntos_gps_colectivo}),
            stroke: new ol.style.Stroke({color: color_borde_puntos_gps_colectivo, width: 1})
            }))
        });

    recorridoPointStyle = new ol.style.Style(
        {
        image: new ol.style.Circle( /** @type {olx.style.IconOptions} */ ({
            radius: 4,
            fill: new ol.style.Fill({color: color_relleno_recorridook_colectivo}),
            stroke: new ol.style.Stroke({color: color_borde_recorridook_colectivo, width: 1})
            }))
        });

    desvioStyle = new ol.style.Style(
        {
        image: new ol.style.Circle( /** @type {olx.style.IconOptions} */ ({
            radius: 4,
            fill: new ol.style.Fill({color: color_relleno_desvios_colectivo}),
            stroke: new ol.style.Stroke({color: color_borde_desvios_colectivo, width: 1})
            }))
        });

    //Umbral definido por defecto
    var umbral = 100;

    //Estilo dinamico dependiente del feature a dibujar
    getUmbralStyle = function(feature) {
        var id = feature.getId();
        return feature.values_.desvio > umbral ? desvioStyle : recorridoPointStyle
      };

    /**
    * Fin de definicion de estilos para el mapa
    **/

    /**
    * Inicializacion de MAPA
    **/
    mapLayers = [];
    streetLayer = new ol.layer.Tile({
    // mirar aca: https://blog.programster.org/openlayers-3-using-different-osm-tiles
      source: new ol.source.OSM({
            'url': 'http://{a-c}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png})'
            }),
       // opacity: 0.4,
    });

    //Configuracion de layers a mostrar
    var customLayers = [];
    recorridoLineLayer = {
        'name': 'Linea Recorrido',
        'search_name': 'recorridoLineLayer',
        'z_index': 0,
        'layer_obj': undefined
    };
    customLayers.push(recorridoLineLayer);

    viajeLayer = {
        'name':'Puntos Viaje',
        'search_name':'viajeLayer',
        'z_index':1,
        'layer_obj': undefined
    };
    customLayers.push(viajeLayer);

    recorridoLayer = {
        'name':'Puntos Recorrido',
        'search_name':'recorridoLayer',
        'z_index':2,
        'layer_obj': undefined
    };

    var layerToStyles =[];

    layerToStyles['recorridoLineLayer'] = trayectoriaStyle
    layerToStyles['viajeLayer'] = pointStyle
    layerToStyles['recorridoLayer'] = getUmbralStyle

    customLayers.push(recorridoLayer);

    mapLayers.push(streetLayer);

    var map = new ol.Map({
      target: 'map',
      controls: ol.control.defaults().extend([
          new ol.control.FullScreen()
        ]),
      layers: mapLayers,
      view: new ol.View({
          projection: 'EPSG:4326',
          center: [-64.166031, -31.48155],
          zoom: 12
        })
    });

    map.on('pointermove', showInfo);

    var info = document.getElementById('info');
    var LayerList = $("#layers-list");
    //drawLayersList();
     /**
    * FIN Inicializacion de MAPA
    **/
    function _toggleMapInfo(){
        $("#info").toggle();
    }
    /*Funciones sobre mapa y layers*/

    function _getZIndexByLayerName(layerName){
        ret = undefined;
        for (var element in customLayers)
        {
            if (customLayers[element]['search_name'] ===layerName){
                ret = customLayers[element]['z_index'];
                break;
            }

        }
        return ret;
    }

    function _setLayerObject(layerName, layer){

        ret = undefined;
        for (var element in customLayers)
        {
            if (customLayers[element]['search_name'] ===layerName){
                customLayers[element]['layer_obj'] = layer;
                break;
            }
        }
    }

    function getMapLayerByName(layerName){
         map.getLayers().forEach(function (layer) {
            var currentLayerName = layer.get('name');
            if (currentLayerName != undefined )
            {
                if (currentLayerName === layerName) return layer;
            }
         });
         return undefined;
    }

    function removeMapLayerByName(layerName){
        var layersToRemove = [];
        map.getLayers().forEach(function (layer) {
            if (layer.get('name') != undefined )
            {
                console.log(layer.get('name') + ' zindex:' + String(layer.getZIndex()));
            }

            if (layer.get('name') != undefined && layer.get('name') === layerName) {
                layersToRemove.push(layer);
            }
        });
        var len = layersToRemove.length;
        for(var i = 0; i < len; i++) {
            map.removeLayer(layersToRemove[i]);
        }
    }



    function _addLayerMap(features, layerName, zIndex){

        style = layerToStyles[layerName];

        largo = 'Desconocido'
        if (features.geometry) {
            largo = 'Geometries: ' + features.geometry.coordinates.length;
        } else if (features.features) {
            largo = 'Features: ' +  features.features.length;
        };

        console.log('Dibujando capa ' + layerName + ' (' + largo + ' features)')
        VectorSource = new ol.source.Vector({
          features: (new ol.format.GeoJSON()).readFeatures(features)
        });

        VectorLayer = new ol.layer.Vector({
            source: VectorSource,
            style: style
        });

        //mapLayers.push(vectorLayer);
        removeMapLayerByName(layerName);


        map.addLayer(VectorLayer);
        VectorLayer.set('name', layerName);
        VectorLayer.setZIndex(zIndex);

        return VectorLayer;
    }

    function showInfo(event) {
        map.forEachFeatureAtPixel(
            event.pixel,
            function(feature, layer){
                var properties = feature.getProperties();
                elem = {
                    'desvio': properties['desvio'],
                    'latitud':properties.geometry.flatCoordinates[1],
                    'longitud':properties.geometry.flatCoordinates[0],
                };
                _mapInfo['punto_recorrido'] = elem;
                _reloadMapInfo();
            },
            {
                'layerFilter': function(layerCandidate){
                    return layerCandidate.get('name')==="recorridoLayer"
                    },
                'hitTolerance':0
            }
        );
    }

    function _getMapInfo(){
        return _mapInfo;
    }

    function _setMapInfo(info){
        _mapInfo = info;
    }

    function _reloadMapInfo(){
        info.innerText = JSON.stringify(_mapInfo, null, 1);
        info.style.opacity = 1;
    }

    function _refeshMap(){
//        map.render();
//        alert("despues de render");

        layerName = "recorridoLayer";
        for (layerIndex in customLayers){
            layer = customLayers[layerIndex];
            if (layer.search_name === layerName){
                source =layer.layer_obj.getSource()
            }
        }
        source.changed();

    }

    /**
    * Dibuja los layers respetando el orden en que se encuentran en customLayers
    * el html deberia venir de algun innerhtml para no tener que estar tocando aca
    **/
    function drawLayersList()
    {
        customLayers.sort((a,b) => (a['z_index']> b['z_index']) ? 1 : ((b['z_index']> a['z_index']) ? -1 : 0));

        for (layerIndex in customLayers){
            layer = customLayers[layerIndex];


            var html = '<li class="layer-item list-group-item">' + customLayers[layerIndex]['name'] + '</li>';
            LayerList.append(html);
        }
    }

    function _setUmbral(valor){
        umbral = valor;
    }
});