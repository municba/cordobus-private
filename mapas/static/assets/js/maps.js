var map = new ol.Map({
    controls: ol.control.defaults().extend([
      new ol.control.FullScreen()
    ]),
    layers: [
        new ol.layer.Tile({
            source: new ol.source.OSM({
                    'url': 'http://{a-c}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png})'
                    }),
               // opacity: 0.4,
        })
    ],
    target: document.getElementById('map'),
    view: new ol.View({
        center: ol.proj.fromLonLat([-64.1862, -31.4074]),
        zoom: 12.5
    })
});

styleCache = {}
function styleFunction(feature) {

    var bondi = feature.get('nombre');
    var style = styleCache[bondi];
    if (style) return [style];
    else {
        var linea = feature.get('linea');
        var color = (feature.get('color').replace('#',''));
        styleCache[bondi] = new ol.style.Style(
            {
    /*        image: new ol.style.Circle(({
                radius: 4,
                fill: new ol.style.Fill({color: feature.get('color')}),
                stroke: new ol.style.Stroke({color: feature.get('color'), width: 1})
                }))*/
                image: new ol.style.Icon({
    /*              anchor: [0.5, 0.5],
                  size: [52, 52],
                  offset: [52, 0],
                  opacity: 1,
                  scale: 0.25,*/
                  crossOrigin: 'anonymous',
                  src: 'https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bb|'+linea+'|'+color+'|000000'
                })
            });

        return [ styleCache[bondi] ];
    }
}

function styleRecorrido(feature, resolution) {
    linea = feature.get('linea');

    var styles = [
        new ol.style.Style({
            stroke: new ol.style.Stroke({
              color: linea['color'],
              width: 2
            }),
        }),
    ];

    return styles;
}

const url = '/tracking/api/internos-activos-ahora';

function cargar_recorrido(next, recorrido) {
    const id_recorrido = recorrido
    fetch(next.replace('http', 'https')).then(function(response) {
        return response.json();
    }).then(function(json) {
        let datos_a_procesar = '';
        if (id_recorrido == 'TODOS')
        {
            datos_a_procesar = json.results;
        }
        else
        {
            datos_a_procesar = json;
        }
        if(datos_a_procesar != undefined)
        {
            var vectorSource = new ol.source.Vector({
                features: (new ol.format.GeoJSON()).readFeatures(datos_a_procesar, {
                    featureProjection:"EPSG:3857"
                })
            });

            var vectorLayer = new ol.layer.Vector({
                source: vectorSource,
                style: styleRecorrido
            });

            map.addLayer(vectorLayer);

            /* identifico la capa con un nombre */
            vectorLayer.set('name', 'recorrido');
        }

        if (json.next != null)
        {
            cargar_recorrido(json.next, id_recorrido);
        }

        /*se cargan los activos*/
        let url_internos_activos = '';
        if (id_recorrido == 'TODOS') {
            url_internos_activos = url;
            cargar_activos(url_internos_activos);
        }
        else {
            url_internos_activos = url + '?id_recorrido=' + id_recorrido;
            cargar_activos(url_internos_activos);
        }
        sessionStorage.setItem('url_internos_activos', url_internos_activos);
    });
}

function cargar_activos(next) {
    /* fixme: una forma muy burda de hacer un request seguro */
    fetch(next.replace('http', 'https')).then(function(response) {
        return response.json();
    }).then(function(json) {

        if(json.results != undefined)
        {

            var vectorSource = new ol.source.Vector({
                features: (new ol.format.GeoJSON()).readFeatures(json.results, {
                    featureProjection:"EPSG:3857"
                })
            });

            var vectorLayer = new ol.layer.Vector({
                source: vectorSource,
                style: styleFunction
            });

            map.addLayer(vectorLayer);

            /* identifico la capa con un nombre */
            vectorLayer.set('name', 'tracking');
        }

        if (json.next != null)
        {
            cargar_activos(json.next)
        }
    });
}

function vector_exists() {
    const layers = map.getLayers().getArray();
    for (let index = 0; index < layers.length; index++)
    {
        const layer = layers[index];
        if (layer.getType() === 'VECTOR')
        {
            return true;
        }
    }

    return false;
}

function limpiar_mapa() {

    while (vector_exists()){
        map.getLayers().forEach(function (layer) {
            if (layer != undefined) {

                if (layer.getType() === 'VECTOR') {

                    map.removeLayer(layer);
                }
            }
        });
    }
}

function actualizar_tracking() {

    while (vector_exists()){
        map.getLayers().forEach(function (layer) {
            if (layer != undefined) {
                console.log('layer.getType(): ', layer.getType());
                console.log('propiedades');
                console.log(layer.get('name'));
                console.log('geometria:');
                console.log(layer.source);

/*                if (layer.getType() === 'VECTOR') {
                }
*/
                /* se elimina la capa de tracking para actualizar los valores */
                if (layer.get('name') == 'tracking') {
                    console.log("eliminando....");
                    map.removeLayer(layer);
                }
            }
        });
    }

    let url_internos_activos = sessionStorage.getItem('url_internos_activos');
    console.log("debería actualizar a:");
    console.log(url_internos_activos);
    cargar_activos(url_internos_activos);
}

var element = document.getElementById('popup');

var popup = new ol.Overlay.Popup({
    element: element,
    positioning: 'bottom-center',
    stopEvent: false,
    offset: [0, -8]
});
map.addOverlay(popup);

// display popup on click
map.on('pointermove', function(evt) {
/*    console.log("onclick: ", evt.pixel);*/

    /* Detecta el feature en el mapa */
    var feature = map.forEachFeatureAtPixel(evt.pixel,
        function(feature) {
            return feature;
        });

    if (feature){
        var coordinates = evt.coordinate;

        var nombre = feature.get('nombre');
        var linea = feature.get('linea');
        var recorrido = feature.get('recorrido_nombre');
        var velocidad = feature.get('ultimas_velocidades_promedio');
        var momento = feature.get('ultima_posicion_time');

        popup.show(coordinates,
            "<strong>" + "Interno: " + nombre + "<br>" +
            "Linea: " + linea + "<br>" +
            "Recorrido: " + recorrido + "<br>" +
            "Velocidad: " + velocidad + "<br>" +
            "Momento: " + momento +
            "</strong>");
    }
    else{
        popup.hide();
    }
});

// change mouse cursor when over marker
map.on('pointermove', function(e) {
    if (e.dragging) {
        popup.hide();
        return;
    }
    var pixel = map.getEventPixel(e.originalEvent);
    var hit = map.hasFeatureAtPixel(pixel);

    map.getTarget().style.cursor = hit ? 'pointer' : '';
});

/*Recargar mapa*/
/*$( document ).ready(function() {
    setInterval("actualizar_tracking()", 30000);
});
*/