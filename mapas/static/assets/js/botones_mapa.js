$( document ).ready(function() {

    $("#btnViajes").click(function () {
        const url = '/lineas/api/recorrido/'
        var recorrido_seleccionado = $("#recorrido :selected").val();
        if (recorrido_seleccionado == 'TODOS') {
            cargar_recorrido(url, recorrido_seleccionado);
        }
        else {
            cargar_recorrido(url + recorrido_seleccionado, recorrido_seleccionado);
        }

    });
});