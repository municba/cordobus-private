from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework_gis.serializers import GeoFeatureModelSerializer, ModelSerializer
from rest_framework import serializers
from lineas.models import Recorrido, Linea, Troncal, Empresa
from tracking.models import InternoGPS



class EmpresaSerializer(CachedSerializerMixin):

    class Meta:
        model = Empresa
        fields = ['id', 'nombre_publico']


class ActivosPorLineaSerializer(ModelSerializer):

    empresa = serializers.StringRelatedField()
    troncal = serializers.StringRelatedField()
    activos = serializers.SerializerMethodField()

    def get_activos(self, obj):
        return InternoGPS.objects.activos_ahora().filter(recorrido_actual__linea=obj).count()

    class Meta:
        model = Linea
        fields = ['id', 'empresa', 'troncal', 'nombre_publico', 'activos']


class LineaSerializer(CachedSerializerMixin):

    empresa = serializers.StringRelatedField()
    troncal = serializers.StringRelatedField()
    color = serializers.CharField(source='empresa.color')

    class Meta:
        model = Linea
        fields = ['id', 'empresa', 'troncal', 'nombre_publico', 'color']


class TroncalSerializer(CachedSerializerMixin):

    lineas = serializers.SerializerMethodField()

    class Meta:
        model = Troncal
        fields = ['id', 'nombre_publico', 'lineas']

    def get_lineas(self, obj):
        lineas = Linea.objects.filter(troncal=obj, publicado=True)
        serializer = LineaSerializer(instance=lineas, many=True)
        return serializer.data

class RecorridoSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):

    linea = LineaSerializer(read_only=True)
    descripcion_cuando_llega = serializers.CharField(source='descripcion_interna')

    class Meta:
        model = Recorrido
        geo_field = 'trazado'
        fields = ['id', 'linea', 'descripcion_corta', 'descripcion_larga', 'trazado', 'descripcion_cuando_llega']

class RecorridoSinGeoSerializer(CachedSerializerMixin):

    linea = LineaSerializer(read_only=True)
    descripcion_cuando_llega = serializers.CharField(source='descripcion_interna')

    class Meta:
        model = Recorrido
        fields = ['id', 'linea', 'descripcion_corta', 'descripcion_larga', 'descripcion_cuando_llega']

class RecorridoExtendidoSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):

    linea = LineaSerializer(read_only=True)

    class Meta:
        model = Recorrido
        geo_field = 'trazado_extendido'
        fields = ['id', 'linea', 'descripcion_corta', 'descripcion_larga', 'trazado_extendido', 'descripcion_interna']


# Registro los serializadores en la cache de DRF
cache_registry.register(LineaSerializer)
cache_registry.register(RecorridoSerializer)
cache_registry.register(RecorridoExtendidoSerializer)
cache_registry.register(RecorridoSinGeoSerializer)
cache_registry.register(TroncalSerializer)
cache_registry.register(EmpresaSerializer)
