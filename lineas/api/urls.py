from django.conf.urls import url, include
from rest_framework import routers
from .views import (LineaViewSet, RecorridoViewSet,
    RecorridoExtendidoViewSet, TroncalViewSet, ActivosPorLineaViewSet, EmpresaViewSet)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'linea', LineaViewSet, base_name='linea.api.lista')
router.register(r'activos-por-linea', ActivosPorLineaViewSet, base_name='activos.linea.api.lista')
router.register(r'recorrido', RecorridoViewSet, base_name='recorrido.api.lista')
router.register(r'recorrido-extendido', RecorridoExtendidoViewSet, base_name='recorridoextendido.api.lista')
router.register(r'lineas-troncales', TroncalViewSet, base_name='troncal.api.lista')
router.register(r'empresas', EmpresaViewSet, base_name='empresa.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
