from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import (LineaSerializer, RecorridoExtendidoSerializer,
    RecorridoSerializer, TroncalSerializer, ActivosPorLineaSerializer, EmpresaSerializer)
from core.pagination import DefaultPagination, BondiMapaPagination
from lineas.models import Linea, Recorrido, Troncal, Empresa



class EmpresaViewSet(viewsets.ModelViewSet):
    """
    Empresa de la ciudad de Cordoba.
    """
    serializer_class = EmpresaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Empresa.objects.all()

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ActivosPorLineaViewSet(viewsets.ModelViewSet):
    """
    Lineas del transporte público de toda la ciudad de Córdoba.

    Se puede filtrar por troncal. Ej:
    id_troncal=1, 2, 3
    """

    serializer_class = ActivosPorLineaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Linea.objects.filter(publicado=True, troncal__isnull=False)

        id_troncal = self.request.query_params.get('id_troncal', None)
        if id_troncal:
            ids = id_troncal.split(',')
            queryset = queryset.filter(troncal__in=ids)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class LineaViewSet(viewsets.ModelViewSet):
    """
    Lineas del transporte público de toda la ciudad de Córdoba.

    Se puede filtrar por troncal. Ej:
    id_troncal=1, 2, 3
    """

    serializer_class = LineaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Linea.objects.filter(publicado=True, troncal__isnull=False)

        id_troncal = self.request.query_params.get('id_troncal', None)
        if id_troncal:
            ids = id_troncal.split(',')
            queryset = queryset.filter(troncal__in=ids)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class RecorridoViewSet(viewsets.ModelViewSet):
    """
    Recorrido de todas las líneas de transporte de la ciudad.
    Se puede filtrar por id del recorrido con el parámetro 'id'. Por ejemplo:
    "id=1". Pueden ser varias ids separadas por comas.
    Se puede filtrar por línea con el parametro 'linea'. Por ejemplo:
    "linea=15". Pueden ser varias líneas, separadas por comas.
    Se puede filtrar por empresa con el parámetro 'e'. Por ejemplo:
    "e=CONIFERAL"
    Se puede filtrar por texto con el parámetro 'q'. Por ejemplo:
    "q=FERREYRA"
    """
    serializer_class = RecorridoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # Filtramos por recorrido publicado
        queryset = Recorrido.objects.filter(publicado=True, trazado__isnull=False)

        id_r = self.request.query_params.get('id', None)
        if id_r is not None:
            id_r = id_r.split(',')
            queryset = queryset.filter(id__in=id_r)

        line = self.request.query_params.get('linea', None)
        if line is not None:
            line = line.split(',')
            queryset = queryset.filter(linea__nombre_publico__in=line)

        e = self.request.query_params.get('e', None)
        if e is not None:
            queryset = queryset.filter(linea__empresa__nombre_publico__icontains=e)

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(descripcion_larga__icontains=q)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class RecorridoExtendidoViewSet(viewsets.ModelViewSet):
    """
    Recorrido de todas las líneas de transporte de la ciudad.
    Se puede filtrar por id del recorrido con el parámetro 'id'. Por ejemplo:
    "id=1". Pueden ser varias ids separadas por comas.
    Se puede filtrar por línea con el parametro 'linea'. Por ejemplo:
    "linea=15". Pueden ser varias líneas, separadas por comas.
    Se puede filtrar por empresa con el parámetro 'e'. Por ejemplo:
    "e=CONIFERAL"
    Se puede filtrar por texto con el parámetro 'q'. Por ejemplo:
    "q=FERREYRA"
    """
    serializer_class = RecorridoExtendidoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # Filtramos por recorrido publicado
        queryset = Recorrido.objects.filter(publicado=True, trazado__isnull=False)

        id_r = self.request.query_params.get('id', None)
        if id_r is not None:
            id_r = id_r.split(',')
            queryset = queryset.filter(id__in=id_r)

        line = self.request.query_params.get('linea', None)
        if line is not None:
            line = line.split(',')
            queryset = queryset.filter(linea__nombre_publico__in=line)

        e = self.request.query_params.get('e', None)
        if e is not None:
            queryset = queryset.filter(linea__empresa__nombre_publico__icontains=e)

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(descripcion_larga__icontains=q)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class TroncalViewSet(viewsets.ModelViewSet):
    """
    Troncales correspondientes a las líneas del transporte público de toda la ciudad.
    """
    serializer_class = TroncalSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Troncal.objects.all()

        return queryset.order_by('nombre_publico')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)