"""

Specs: https://developers.google.com/transit/gtfs/reference/?hl=es-419#agencytxt

agency_id	Opcional	El campo agency_id es un ID que identifica de forma exclusiva a una empresa de transporte público. Un feed de transporte público puede representar datos de más de una empresa. El agency_id es un conjunto único de datos. Este campo es opcional para los feeds de transporte público que solo incluyen datos de una sola empresa.
agency_name	Obligatorio	El campo agency_name incluye el nombre completo de la empresa de transporte público. Google Maps mostrará este nombre.
agency_url	Obligatorio	El campo agency_url incluye la URL de la empresa de transporte público. El valor debe ser una URL que cumpla todos los requisitos y que incluya http:// o https://. Y todos los caracteres especiales incluidos en la URL deben tener el formato de escape correcto. Para obtener una descripción que explique cómo crear valores de URL que cumplan todos los requisitos, consulta http://www.w3.org/Addressing/URL/4_URI_Recommentations.html.
agency_timezone	Obligatorio	El campo agency_timezone incluye la zona horaria del lugar en el que se encuentra la empresa de transporte público. Los nombres de zona horaria no pueden incluir el carácter de espacio, aunque sí un guion bajo. Para obtener una lista de los valores válidos, consulta http://en.wikipedia.org/wiki/List_of_tz_zones. Si se especifican varias empresas en el feed, cada una debe tener el mismo valor de agency_timezone.
agency_lang	Opcional	El campo agency_lang contiene un código de idioma IETF BCP 47 que especifica el idioma principal utilizado por esta empresa de transporte público. Esta configuración ayuda a los usuarios de la especificación GTFS a elegir las reglas sobre el uso de mayúsculas y otros parámetros específicos de la configuración de idioma para el feed. Si deseas obtener una introducción sobre el código IETF BCP 47, consulta http://www.rfc-editor.org/rfc/bcp/bcp47.txt y http://www.w3.org/International/articles/language-tags/.
agency_phone	Opcional	El campo agency_phone incluye un solo número de teléfono para la empresa especificada. Este campo es un valor de cadena que presenta el número de teléfono correspondiente al área de servicio de la empresa. Puede y debe incluir signos de puntuación para agrupar los dígitos del número. Se permite el uso de texto de marcación (por ejemplo, "503-238-RIDE" de TriMet), pero el campo no debe incluir ningún otro texto descriptivo.
agency_fare_url	Opcional	El campo agency_fare_url especifica la URL de una página web que permite que un pasajero compre boletos o cualquier otro instrumento de pasaje en esa empresa en línea. El valor debe ser una URL que cumpla todos los requisitos y que incluya http:// o https://. Y todos los caracteres especiales incluidos en la URL deben tener el formato de escape correcto. Para obtener una descripción que explique cómo crear valores de URL que cumplan todos los requisitos, consulta http://www.w3.org/Addressing/URL/4_URI_Recommentations.html.
agency_email	Opcional

"""
import django_excel as excel
from lineas.models import Empresa
from django.conf import settings
import os
from django.views.decorators.cache import cache_page
import shutil


def gen_agency_txt(dest_file='agency'):
    ''' generar el contenido CSV y grabarlo a disco.
        Devuelve el objeto.
    '''
    csv_list = []
    headers = ['agency_id', 'agency_name', 'agency_url',
                'agency_timezone', 'agency_lang', 'agency_phone',
                'agency_fare_url', 'agency_email']
    csv_list.append(headers)

    agencies = Empresa.objects.all()

    
    for agency in agencies:
        url = '' if agency.url is None else agency.url
        telefono = '' if agency.telefono is None else agency.telefono
        email = '' if agency.email is None else agency.email
        lst = [agency.id,
                agency.nombre_publico,
                url,
                'America/Argentina/Cordoba',
                'es',  # no funcionan: 'es-419', 'es-ar'
                telefono,
                '',
                email
                ]

        csv_list.append(lst)

    # guardar el archivo para zomprimir y futuras referencias
    dest_fold = os.path.join(settings.MEDIA_ROOT, 'gtfs')
    if not os.path.exists(dest_fold):
        os.makedirs(dest_fold)
    dest_csv = os.path.join(dest_fold, dest_file + '.csv')
    sheet = excel.pe.Sheet(csv_list)
    sheet.save_as(filename=dest_csv)
    shutil.copyfile(dest_csv, dest_csv.replace('.csv', '.txt'))

    return sheet
    

@cache_page(60 * 60 * 24)  # 1 h
def gtfs_agency(request):
    '''
    agency.txt from GTFS spec
    '''
    sheet = gen_agency_txt()
    return excel.make_response(sheet, 'csv')