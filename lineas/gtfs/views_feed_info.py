"""
Feed Info
Specs: https://developers.google.com/transit/gtfs/reference/

"""
import django_excel as excel
from django.conf import settings
from django.views.decorators.cache import cache_page
import shutil
import os


def gen_feed_info_txt(dest_file='feed_info'):
    ''' generar el contenido CSV y grabarlo a disco.
        Devuelve el objeto.
    '''
    csv_list = []
    headers = ['feed_publisher_name', 'feed_publisher_url', 'feed_lang',
                'feed_start_date', 'feed_end_date', 'feed_version', 
                'feed_contact_email', 'feed_contact_url'
                ]
    csv_list.append(headers)

    csv_list.append(['Municipalida de Córdoba, Argentina',
                        'https://www.cordoba.gob.ar/',
                        'es',
                        '20190520',
                        '20200520',
                        'v0.1.0',
                        'gobiernoabiertocba@gmail.com',
                        'https://gobiernoabierto.cordoba.gob.ar/'
                        ])

    

    # guardar el archivo para zomprimir y futuras referencias
    dest_fold = os.path.join(settings.MEDIA_ROOT, 'gtfs')
    if not os.path.exists(dest_fold):
        os.makedirs(dest_fold)
    dest_csv = os.path.join(dest_fold, dest_file + '.csv')
    sheet = excel.pe.Sheet(csv_list)
    sheet.save_as(filename=dest_csv)
    shutil.copyfile(dest_csv, dest_csv.replace('.csv', '.txt'))

    return sheet
    

@cache_page(60 * 60 * 24)  # 1 h
def gtfs_feed_info(request):
    '''
    agency.txt from GTFS spec
    '''
    sheet = gen_feed_info_txt()
    return excel.make_response(sheet, 'csv')