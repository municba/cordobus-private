"""

Specs: https://developers.google.com/transit/gtfs/reference/?hl=es-419#stopstxt


"""
import django_excel as excel
from lineas.models import Recorrido
from django.conf import settings
import os
from django.views.decorators.cache import cache_page
import shutil


def gen_shapes_txt(dest_file='shapes'):
    csv_list = []
    headers = ['shape_id', 'shape_pt_lat', 'shape_pt_lon',
                'shape_pt_sequence', 'shape_dist_traveled'
                ]
    csv_list.append(headers)

    recorridos = Recorrido.objects.filter(publicado=True, linea__publicado=True).exclude(descripcion_corta='').exclude(trazado__isnull=True)

    for recorrido in recorridos:

        c = 0
        if recorrido.trazado_extendido is not None:
            for punto in recorrido.trazado_extendido:
                lst = [recorrido.id]
                lst += [punto[1], punto[0], c, '']

                c += 1
            

                csv_list.append(lst)


    # guardar el archivo para zomprimir y futuras referencias
    dest_fold = os.path.join(settings.MEDIA_ROOT, 'gtfs')
    if not os.path.exists(dest_fold):
        os.makedirs(dest_fold)
    dest_csv = os.path.join(dest_fold, dest_file + '.csv')
    sheet = excel.pe.Sheet(csv_list)
    sheet.save_as(filename=dest_csv)

    shutil.copyfile(dest_csv, dest_csv.replace('.csv', '.txt'))

    return sheet



@cache_page(60 * 60 * 24)  # 1 h
def gtfs_shapes(request):
    '''
    routes.txt from GTFS spec
    '''
    sheet = gen_shapes_txt()
    return excel.make_response(sheet, 'csv')