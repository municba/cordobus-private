"""
Specs: https://developers.google.com/transit/gtfs/reference/?hl=es#frequenciestxt
"""
import django_excel as excel
from lineas.models import Linea, Recorrido
from colectivos.models import (EstacionAnioFrecuencias, DiasGrupoFrecuencias,
    FranjaHorariaFrecuencias, AgrupadorFranjaHoraria, Frecuencia)
from django.conf import settings
import os
from django.views.decorators.cache import cache_page
import shutil


def gen_frequencies_txt(dest_file='frequencies'):
    csv_list = []
    headers = ['trip_id', 'start_time', 'end_time', 'headway_secs',
                'exact_times'
                ]
    csv_list.append(headers)

    recorridos = Recorrido.objects.filter(publicado=True, linea__publicado=True).exclude(descripcion_corta='').exclude(trazado__isnull=True)
    horarios = FranjaHorariaFrecuencias.objects.all()

    for recorrido in recorridos:

        for horario in horarios:
            trip = '{}_{}'.format(recorrido.id, horario.id)
            try:
                frecuencia_esperada = Frecuencia.objects.get(linea=recorrido.linea, agrupador=horario.agrupador).frecuencia_en_minutos_esperada
            except Exception as e:
                print(e)
                print('intentando buscar frecuencia con linea {} y agrupador {}'.format(recorrido.linea, horario.agrupador))
                continue
            if frecuencia_esperada > 0 and recorrido.paradas.all().count() > 0:
                lst = [trip,
                        horario.hora_desde.strftime("%H:%M:%S"), # start_time: horario del inicio de la frecuencia
                        horario.hora_hasta.strftime("%H:%M:%S"), # end_time: horario
                        frecuencia_esperada*60, # headway_secs: periodo de salida entre la misma parada
                        0 # exact_time: 0 indica que no estan programados de manera exacta
                        ]

                csv_list.append(lst)

    # guardar el archivo para comprimir y futuras referencias
    dest_fold = os.path.join(settings.MEDIA_ROOT, 'gtfs')
    if not os.path.exists(dest_fold):
        os.makedirs(dest_fold)
    dest_csv = os.path.join(dest_fold, dest_file + '.csv')

    sheet = excel.pe.Sheet(csv_list)
    sheet.save_as(filename=dest_csv)
    shutil.copyfile(dest_csv, dest_csv.replace('.csv', '.txt'))

    return sheet


@cache_page(60 * 60 * 24)  # 1 h
def gtfs_frequencies(request):
    """
    frequencies.txt from GTFS spec
    """
    sheet = gen_frequencies_txt()
    return excel.make_response(sheet, 'csv')