"""

Specs: https://developers.google.com/transit/gtfs/reference/?hl=es-419


"""
import django_excel as excel
from paradas.models import Parada
from lineas.models import Recorrido
from colectivos.models import FranjaHorariaFrecuencias
from django.conf import settings
import os
from django.views.decorators.cache import cache_page
import shutil
from datetime import datetime, timedelta
import random
# from django.contrib.gis.db.models.functions import Distance
# from django.contrib.gis.measure import D, Distance
from geopy import distance


def gen_stop_times_txt(dest_file='stop_times'):
    csv_list = []
    headers = ['trip_id', 'arrival_time', 'departure_time', 'stop_id', 'stop_sequence',
                'stop_headsign', 'pickup_type', 'drop_off_type', 'shape_dist_traveled',
                'timepoint'
     ]
    csv_list.append(headers)

    recorridos = Recorrido.objects.filter(publicado=True, linea__publicado=True).exclude(descripcion_corta='').exclude(trazado__isnull=True)
    horarios = FranjaHorariaFrecuencias.objects.all()

    total_recorridos = recorridos.count()
    total_horarios = horarios.count()
    r = 0
    CONST_SPEED_MS = 3  # 3 m/s default speed (~10 km/h)
    for recorrido in recorridos:
        r += 1
        h = 0
        for horario in horarios:
            h += 1

            #FIXME cada rango horario incluye varias horas y varios colectivos
            # aqui solo se registra un colectivo por cada franja horaria
            # en teoría se usa el archivo frecuencies en lugar de este. Ver si es realmente así

            c = 0

            trip = '{}_{}'.format(recorrido.id, horario.id)
            dep_tm = datetime(2019,1,1,0,random.randint(4, 6),random.randint(1, 59))
            last_poste = None
            distancia_entre_paradas = 1
            for parada in recorrido.paradas.filter(verificado=True).order_by('poste__id').distinct():

                # ver la distancia con el poste anterior
                if last_poste is None:
                    seconds = 1
                else:
                    # distancia_entre_paradas = Distance(parada.poste.ubicacion, last_poste.ubicacion)
                    # distancia_entre_paradas = parada.poste.ubicacion.distance(last_poste.ubicacion)
                    distancia_entre_paradas = distance.distance(parada.poste.ubicacion, last_poste.ubicacion).m
                    # print('Dist {}'.format(distancia_entre_paradas))
                    if distancia_entre_paradas > 5000:
                        print('{}/{} {}/{} {} Paradas MUY alejadas: {} mts. {} {} {} {} ({})'.format(r, total_recorridos, h, total_horarios, c, distancia_entre_paradas, recorrido.linea.nombre_publico, recorrido.descripcion_interna, horario.nombre, parada.codigo, parada.poste.id_externo))
                        speed_ms = 20  # va más rápido para que no se pase la cantidad de segundos a un numero muy alto
                    elif distancia_entre_paradas > 700:
                        print('{}/{} {}/{} {} Paradas --- alejadas: {} mts. {} {} {} {} ({})'.format(r, total_recorridos, h, total_horarios, c, distancia_entre_paradas, recorrido.linea.nombre_publico, recorrido.descripcion_interna, horario.nombre, parada.codigo, parada.poste.id_externo))
                        speed_ms = 10  # va más rápido para que no se pase la cantidad de segundos a un numero muy alto
                    else:
                        speed_ms = CONST_SPEED_MS
                    
                    seconds = distancia_entre_paradas / speed_ms

                # esto se va a ignorar en el feed porque están las frecuencias
                arr_tm = dep_tm + timedelta(seconds=seconds)

                lst = [trip,
                        arr_tm.strftime("%H:%M:%S"),  # arrival_time
                        arr_tm.strftime("%H:%M:%S"),  # departure_time
                        parada.poste.id, 
                        c, 
                        '', 
                        0,
                        0,
                        '', 
                        0,  # los horarios son aproximados
                ]

                c += 1

                csv_list.append(lst)
                dep_tm = arr_tm
                last_poste = parada.poste

    # guardar el archivo para zomprimir y futuras referencias
    dest_fold = os.path.join(settings.MEDIA_ROOT, 'gtfs')
    if not os.path.exists(dest_fold):
        os.makedirs(dest_fold)
    dest_csv = os.path.join(dest_fold, dest_file + '.csv')
    sheet = excel.pe.Sheet(csv_list)
    sheet.save_as(filename=dest_csv)

    shutil.copyfile(dest_csv, dest_csv.replace('.csv', '.txt'))

    return sheet


@cache_page(60 * 60 * 24)  # 1 h
def gtfs_stop_times(request):
    '''
    stops.txt from GTFS spec
    '''
    
    sheet = gen_stop_times_txt()
    return excel.make_response(sheet, 'csv')