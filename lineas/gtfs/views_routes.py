"""

Specs: https://developers.google.com/transit/gtfs/reference/?hl=es-419#stopstxt


"""
import django_excel as excel
from lineas.models import Linea
from django.conf import settings
import os
from django.views.decorators.cache import cache_page
import shutil


def gen_routes_txt(dest_file='routes'):
    csv_list = []
    headers = ['route_id', 'agency_id', 'route_short_name', 'route_long_name',
                'route_desc', 'route_type', 'route_url', 'route_color',
                'route_text_color' #, 'route_sort_order'
                ]
    csv_list.append(headers)

    lineas = Linea.objects.filter(publicado=True).exclude(nombre_publico='')
    for linea in lineas:
        nombre_corto = linea.nombre_publico
        # nombre_largo = '{} {}'.format(linea.nombre_publico, linea.empresa.nombre_publico)

        lst = [linea.id, linea.empresa.id,
                nombre_corto,
                '', # nombre_largo,
                '', # route_desc: descripcion del servicio(en este caso, de la línea)
                3,  # tipo de recorrido: bondi es 3 
                '',  # route_url
                linea.empresa.color.replace('#', ''), # route_color
                '' # route_text_color
                # ''   route_sort_order
                ]

        csv_list.append(lst)

    # guardar el archivo para zomprimir y futuras referencias
    dest_fold = os.path.join(settings.MEDIA_ROOT, 'gtfs')
    if not os.path.exists(dest_fold):
        os.makedirs(dest_fold)
    dest_csv = os.path.join(dest_fold, dest_file + '.csv')
    
    sheet = excel.pe.Sheet(csv_list)
    sheet.save_as(filename=dest_csv)
    shutil.copyfile(dest_csv, dest_csv.replace('.csv', '.txt'))
    
    return sheet


@cache_page(60 * 60 * 24)  # 1 h
def gtfs_routes(request):
    '''
    routes.txt from GTFS spec
    '''
    sheet = gen_routes_txt()
    return excel.make_response(sheet, 'csv')