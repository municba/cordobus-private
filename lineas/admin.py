from django.contrib import admin
from core.admin import QhapaxOSMGeoAdmin
from .models import Empresa, Troncal, Linea, Recorrido


@admin.register(Empresa)
class EmpresaAdmin(admin.ModelAdmin):
    search_fields = ['id', 'id_externo', 'nombre_publico']
    list_display = ('id', 'id_externo', 'color','nombre_publico', 'creado', 'ultima_modificacion')


@admin.register(Troncal)
class TroncalAdmin(admin.ModelAdmin):
    search_fields = ['nombre_publico']
    list_display = ('id', 'nombre_publico')


@admin.register(Linea)
class LineaAdmin(admin.ModelAdmin):
    def empresap(self, obj):
        return obj.empresa.nombre_publico

    search_fields = ['nombre_publico', 'id_externo']
    list_display = ('id', 'nombre_publico', 'empresap', 'troncal', 'publicado', 'tiempo_de_recorrido')
    list_filter = ['publicado', 'troncal', 'empresa']


class PosteParadaTransportePublicoAdmin(QhapaxOSMGeoAdmin):
    search_fields = ['id_externo', 'descripcion', 'calle_principal', 'calle_secundaria']
    list_display = ('id_externo', 'fuente', 'descripcion', 'sentido', 'calle_principal', 'calle_secundaria')
    list_filter = ['fuente']


class ParadaTransportePublicoAdmin(QhapaxOSMGeoAdmin):
    search_fields = ['poste__descripcion', 'linea__nombre_publico',
                        'recorrido__descripcion_corta',
                        'recorrido__descripcion_larga',
                        'recorrido__descripcion_interna']
    list_display = ('poste', 'linea', 'recorrido', 'fuente', 'distancia_a_recorrido', 'verificado')
    list_filter = ['linea', 'fuente', 'recorrido', 'verificado']


@admin.register(Recorrido)
class RecorridoAdmin(QhapaxOSMGeoAdmin):

    def kilometros(self, obj):
        if obj.trazado is None:
            return 0.0
        linea = obj.trazado
        linea.transform(3857)
        return round(linea.length / 1000, 2)  # no estoy 100% seguro de esto

    search_fields = ['id', 'descripcion_corta', 'descripcion_larga', 'linea__nombre_publico']
    list_display = ('id', 'linea', 'kilometros', 'descripcion_corta', 'descripcion_larga', 'publicado', 'descripcion_interna')
    list_filter = ['linea', 'publicado']
